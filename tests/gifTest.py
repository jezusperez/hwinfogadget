from PIL import Image, ImageSequence
import pygame
from pygame.locals import *
import time
import array
import math

# > pip install Pillow
# > pip install pygame

def pilImageToSurface(pilImage):
    mode, size, data = pilImage.mode, pilImage.size, pilImage.tobytes()
    return pygame.image.fromstring(data, size, mode).convert_alpha()

def loadGIF(filename):
    pilImage = Image.open(filename)
    frames = []
    if pilImage.format == 'GIF' and pilImage.is_animated:
        for frame in ImageSequence.Iterator(pilImage):
            pygameImage = pilImageToSurface(frame.convert('RGBA'))
            frames.append(pygameImage)
    else:
        frames.append(pilImageToSurface(pilImage))
    return frames

def renderImage(x, y, width, height, screen, image):
    image = pygame.transform.scale(image, (width, height))
    screen.blit(image, (x, y))

def renderRotatedImage(x, y, width, height, angle, screen, image):
    image = pygame.transform.scale(image, (width, height))
    image_rect = image.get_rect(topleft = (x - (width /2), (y - (height /2))))
    offset_center_to_pivot = pygame.math.Vector2((x, y)) - image_rect.center
    rotated_offset = offset_center_to_pivot.rotate(-angle)
    rotated_image_center = (x - rotated_offset.x, y - rotated_offset.y)
    rotated_image = pygame.transform.rotate(image, angle)
    rotated_image_rect = rotated_image.get_rect(center = rotated_image_center)
    screen.blit(rotated_image, rotated_image_rect)

def main():
    pygame.init()
    screen = pygame.display.set_mode((800, 480))

    animatedBack = loadGIF("hulk.gif")
    animatedBackIndex = 0
    
    while 1:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                return

        renderImage(0, 0, screen.get_width(), screen.get_height(), screen, animatedBack[animatedBackIndex])

    # renderRotatedImage(width / 2, height / 2, screen.get_width(), screen.get_height(), 1, screen, hulk[index])
        

        pygame.display.flip()

        animatedBackIndex = animatedBackIndex + 1;
        if(animatedBackIndex >= len(animatedBack)):
            animatedBackIndex = 0;

        time.sleep(0.05)

if __name__ == "__main__":
    main()
    