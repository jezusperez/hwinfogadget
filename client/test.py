# import psutil
# import platform
# import json
# from datetime import datetime

# def get_size(bytes, suffix="B"):
#     """
#     Scale bytes to its proper format
#     e.g:
#         1253656 => '1.20MB'
#         1253656678 => '1.17GB'
#     """
#     factor = 1024
#     for unit in ["", "K", "M", "G", "T", "P"]:
#         if bytes < factor:
#             return f"{bytes:.2f}{unit}{suffix}"
#         bytes /= factor

# def gethwInformation():
#     cpuInfo = {}
#     memInfo = {}
#     netInfo = {}

#     print("="*40, "System Information", "="*40)
#     uname = platform.uname()
#     print(f"System: {uname.system}")
#     print(f"Node Name: {uname.node}")
#     print(f"Release: {uname.release}")
#     print(f"Version: {uname.version}")
#     print(f"Machine: {uname.machine}")
#     print(f"Processor: {uname.processor}")

#     # Boot Time
#     print("="*40, "Boot Time", "="*40)
#     boot_time_timestamp = psutil.boot_time()
#     bt = datetime.fromtimestamp(boot_time_timestamp)
#     print(f"Boot Time: {bt.year}/{bt.month}/{bt.day} {bt.hour}:{bt.minute}:{bt.second}")

#     # # let's print CPU information
#     # print("="*40, "CPU Info", "="*40)
#     # # number of cores
#     # print("Physical cores:", psutil.cpu_count(logical=False))
#     # print("Total cores:", psutil.cpu_count(logical=True))
#     # CPU frequencies
#     cpufreq = psutil.cpu_freq()
#     # print(f"Max Frequency: {cpufreq.max:.2f}Mhz")
#     # print(f"Min Frequency: {cpufreq.min:.2f}Mhz")
#     # print(f"Current Frequency: {cpufreq.current:.2f}Mhz")
#     # # CPU usage
#     # print("CPU Usage Per Core:")
#     for i, percentage in enumerate(psutil.cpu_percent(percpu=True, interval=1)):
#         # print(f"Core {i}: {percentage}%")
#         key = f"Core {i}"
#         cpuInfo[key] = percentage
#     # print(f"Total CPU Usage: {psutil.cpu_percent()}%")

#     cpuInfo["Current Frequency"] = cpufreq.current
#     cpuInfo["Min Frequency"] = cpufreq.min
#     cpuInfo["Max Frequency"] = cpufreq.max
#     cpuInfo["Total CPU Usage"] = psutil.cpu_percent()
    

#     # # Memory Information
#     # print("="*40, "Memory Information", "="*40)
#     # get the memory details
#     svmem = psutil.virtual_memory()
#     # print(f"Total: {get_size(svmem.total)}")
#     # print(f"Available: {get_size(svmem.available)}")
#     # print(f"Used: {get_size(svmem.used)}")
#     # print(f"Percentage: {svmem.percent}%")
#     # print("="*20, "SWAP", "="*20)
#     # get the swap memory details (if exists)
#     swap = psutil.swap_memory()
#     # print(f"Total: {get_size(swap.total)}")
#     # print(f"Free: {get_size(swap.free)}")
#     # print(f"Used: {get_size(swap.used)}")
#     # print(f"Percentage: {swap.percent}%")

#     memInfo["RAM Total"] = svmem.total
#     memInfo["RAM Available"] = svmem.available
#     memInfo["RAM Used"] = svmem.used
#     memInfo["RAM Percentage"] = svmem.percent
#     memInfo["SWAP Total"] = swap.total
#     memInfo["SWAP Free"] = swap.free
#     memInfo["SWAP Used"] = swap.used
#     memInfo["SWAP Percentage"] = swap.percent
#     # memInfo["SWAP "] = 






#     # Disk Information
#     print("="*40, "Disk Information", "="*40)
#     print("Partitions and Usage:")
#     # get all disk partitions
#     partitions = psutil.disk_partitions()
#     for partition in partitions:
#         print(f"=== Device: {partition.device} ===")
#         print(f"  Mountpoint: {partition.mountpoint}")
#         print(f"  File system type: {partition.fstype}")
#         try:
#             partition_usage = psutil.disk_usage(partition.mountpoint)
#         except PermissionError:
#             # this can be catched due to the disk that
#             # isn't ready
#             continue
#         print(f"  Total Size: {get_size(partition_usage.total)}")
#         print(f"  Used: {get_size(partition_usage.used)}")
#         print(f"  Free: {get_size(partition_usage.free)}")
#         print(f"  Percentage: {partition_usage.percent}%")
#     # get IO statistics since boot
#     disk_io = psutil.disk_io_counters()
#     print(f"Total read: {get_size(disk_io.read_bytes)}")
#     print(f"Total write: {get_size(disk_io.write_bytes)}")

#     # Network information
#     print("="*40, "Network Information", "="*40)
#     # get all network interfaces (virtual and physical)
#     if_addrs = psutil.net_if_addrs()
#     for interface_name, interface_addresses in if_addrs.items():
#         for address in interface_addresses:
#             print(f"=== Interface: {interface_name} ===")
#             if str(address.family) == 'AddressFamily.AF_INET':
#                 print(f"  IP Address: {address.address}")
#                 print(f"  Netmask: {address.netmask}")
#                 print(f"  Broadcast IP: {address.broadcast}")
#             elif str(address.family) == 'AddressFamily.AF_PACKET':
#                 print(f"  MAC Address: {address.address}")
#                 print(f"  Netmask: {address.netmask}")
#                 print(f"  Broadcast MAC: {address.broadcast}")
#     # get IO statistics since boot
#     net_io = psutil.net_io_counters()
#     print(f"Total Bytes Sent: {get_size(net_io.bytes_sent)}")
#     print(f"Total Bytes Received: {get_size(net_io.bytes_recv)}")
#     netInfo["Send"] = net_io.bytes_sent
#     netInfo["Received"] = net_io.bytes_recv





    

#     # GPU information
#     import GPUtil
#     from tabulate import tabulate
#     print("="*40, "GPU Details", "="*40)
#     gpus = GPUtil.getGPUs()
#     list_gpus = []
#     for gpu in gpus:
#         # get the GPU id
#         gpu_id = gpu.id
#         # name of GPU
#         gpu_name = gpu.name
#         # get % percentage of GPU usage of that GPU
#         gpu_load = f"{gpu.load*100}%"
#         # get free memory in MB format
#         gpu_free_memory = f"{gpu.memoryFree}MB"
#         # get used memory
#         gpu_used_memory = f"{gpu.memoryUsed}MB"
#         # get total memory
#         gpu_total_memory = f"{gpu.memoryTotal}MB"
#         # get GPU temperature in Celsius
#         gpu_temperature = f"{gpu.temperature} °C"
#         gpu_uuid = gpu.uuid
#         list_gpus.append((
#             gpu_id, gpu_name, gpu_load, gpu_free_memory, gpu_used_memory,
#             gpu_total_memory, gpu_temperature, gpu_uuid
#         ))

#     print(tabulate(list_gpus, headers=("id", "name", "load", "free memory", "used memory", "total memory",
#                                     "temperature", "uuid")))
#     output = {}
#     output["CPU"] = cpuInfo
#     output["MEM"] = memInfo
#     output["NET"] = netInfo
#     return output

# hwInformation = gethwInformation()
# print(json.dumps(hwInformation, indent=4))















import clr #package pythonnet, not clr
import json
from pathlib import Path

openhardwaremonitor_sensortypes = ['Voltage','Clock','Temperature','Load','Fan','Flow','Control','Level','Factor','Power','Data','SmallData']

def initialize_openhardwaremonitor():
    file = str(Path.cwd()) + r"\OpenHardwareMonitorLib.dll"
   
    clr.AddReference(file)
   
    from OpenHardwareMonitor import Hardware

    handle = Hardware.Computer()
    handle.MainboardEnabled = True
    handle.CPUEnabled = True
    handle.RAMEnabled = True
    handle.GPUEnabled = True
    handle.HDDEnabled = True

    handle.Open()
    return handle

def fetch_stats(handle):
    for hw in handle.Hardware:
        hw.Update()

        print("="*40)
        print(hw)
        print(hw.Name)
        print(hw.Identifier)
        print(hw.HardwareType)

        for sensor in hw.Sensors:
            print(" "*4, sensor)
            print(" "*8, "sensor.Name :", sensor.Name)
            print(" "*8, "sensor.Identifier :", sensor.Identifier)
            print(" "*8, "sensor.SensorType :", sensor.SensorType)
            print(" "*8, "sensor.Value :", sensor.Value)
            print(" "*8, "sensor.Min :", sensor.Min)
            print(" "*8, "sensor.Max :", sensor.Max)

        # for sensor in i.Sensors:
        #     parse_sensor(sensor)
           
        # # for j in i.SubHardware:
        # #     j.Update()
        # #     for subsensor in j.Sensors:
        # #         parse_sensor(subsensor)

def parse_sensor(sensor):
    hardwaretypes = openhardwaremonitor_sensortypes
    print("="*40)
    print("sensor.Name :", sensor.Name)
    print("sensor.Identifier :", sensor.Identifier)
    print("sensor.SensorType :", sensor.SensorType)
    # print("sensor.Parent :", sensor.Parent)
    print("sensor.Value :", sensor.Value)
    print("sensor.Min :", sensor.Min)
    print("sensor.Max :", sensor.Max)
    # print("sensor.HardwareType :", sensor.HardwareType)
    # if sensor.Value is not None:
    #     print(str(sensor.SensorType))
    #     print(sensor)
    #     # print(json.dumps(sensor, indent=4))
    #     # # if str(sensor.SensorType) == 'Temperature':
    #     # #     print(u"%s %s Temperature Sensor #%i %s - %s\u00B0C" % (hardwaretypes[sensor.Hardware.HardwareType], sensor.Hardware.Name, sensor.Index, sensor.Name, sensor.Value))



if __name__ == "__main__":
    print("OpenHardwareMonitor:")
    HardwareHandle = initialize_openhardwaremonitor()
    fetch_stats(HardwareHandle)