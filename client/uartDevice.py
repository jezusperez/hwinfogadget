import os;
from os import listdir;
from os.path import isfile, join;
import time;
import random;
import Adafruit_BBIO.UART as UART
from serial import Serial;
import serial.tools.list_ports as port_list;

ENABLE_LOG = False
def logMessage(*args):
    if ENABLE_LOG is True:
        for arg in args:
            print("uartDevice Module:", arg)

class scrambler:
    bufferSize = 4096;

    # def __init__(self):
    #     pass;

    def __init__(self):
        pass;

    def scramble(self, message):
        buffer = message.encode('utf-8');
        buffer_len = len(message)##.to_bytes(2, 'big');
        logMessage("buffer", buffer)
        logMessage("buffer_len", buffer_len)

        # print(message);
        # print(buffer);

        output = bytearray();
        skey = random.randrange(0, 255, 3)
        output.append(skey);
        for index in range(0, buffer_len, 1):
            output.append(buffer[index] ^ skey)
    
        return output;

    def descramble(self, message):
        outputMessage = bytearray();
        for index in range(1, len(message), 1):
            # print(output[index + 2] ^ output[0]);
            outputMessage.append(message[index] ^ message[0]);
        return bytearray(outputMessage);

class uartDevice:
    serialEnabled = False;
    serialPortName = "";
    serialPortDevice = "";
    serialPortSpeed = "";
    serialPort = None;
    bufferSize = 1024;
    buffer = bytearray(bufferSize);
    dataScrambler = None

    def __init__(self, serialPortName, serialPortDevice, serialPortSpeed):
        self.serialPortName = serialPortName;
        self.serialPortSpeed = serialPortSpeed;
        self.serialPortDevice = serialPortDevice;
        self.serialEnabled = False;
        self.dataScrambler = scrambler()
        logMessage("self.serialPortName", self.serialPortName)
        logMessage("self.serialPortSpeed", self.serialPortSpeed)
        logMessage("self.serialPortDevice", self.serialPortDevice)

    def open(self):
        # try:
        #     UART.setup(self.serialPortName)
        #     self.serialPort = Serial.Serial(port = self.serialPortDevice, 
        #             baudrate=self.serialPortSpeed, 
        #             timeout=0.5, write_timeout=0.5)
        #     self.serialPort.close()
        #     self.serialPort.open()
        #     if self.serialPort.isOpen():
        #         self.serialEnabled = True;
        #     return 0;
        # except:
        #     self.serialEnabled = False;
        #     return 1;
        UART.setup(self.serialPortName)
        self.serialPort = Serial(port = self.serialPortDevice, 
                baudrate=self.serialPortSpeed, 
                timeout=0.5, write_timeout=0.5)
        self.serialPort.close()
        self.serialPort.open()
        if self.serialPort.isOpen():
            self.serialEnabled = True;
            logMessage("self.serialEnabled", self.serialEnabled)
            return 0;
        self.serialEnabled = False;
        logMessage("self.serialEnabled", self.serialEnabled)
        return 1;

    def close(self):
        # self.serialPort.close();
        if(self.serialEnabled == True):
            self.serialPort.close();
            return 0;
        else:
            return 1;
        
    def sendMessage(self, message):
        if(self.serialEnabled == False):
            return 1;
        # print("self.serialEnabled =", self.serialEnabled);
        # print("len(message) =", len(message));
        if(self.serialEnabled == True and len(message) <= self.bufferSize):
            # # print("message =", message);
            # for i in range(0, len(message)):
            #     self.buffer[i] = message[i];
            # # print("self.buffer =", self.buffer);
            # self.serialPort.write(self.buffer);
            self.serialPort.flush()
            self.serialPort.write(self.dataScrambler.scramble(message));
            return 0;
        else:
            return 1;

    def reciveMessage(self):
        if(self.serialEnabled == True):
            inputBuffer = self.serialPort.read(self.bufferSize);
            if len(inputBuffer) <= 1:
                return None;
            # # print("self.bufferSize =", self.bufferSize);
            # # print("len(inputBuffer) =", len(inputBuffer));
            # for i in range(0, self.bufferSize):
            #     # print("i =", i);
            #     self.buffer[i] = inputBuffer[i];
            # return self.buffer;
            self.serialPort.flush()
            return self.dataScrambler.descramble(inputBuffer).decode('utf-8')
        else:
            return None;

if __name__ == "__main__":
    ENABLE_LOG = True
    uart = uartDevice("UART4", "/dev/ttyO4", 115200)
    # uart.open()
    # while 1:
    #     uartDATA = uart.reciveMessage()
    #     print(uartDATA)
    # uart.close()

    uart.open()
    uart.sendMessage("{\"HW\": {\"CPUName\": \"AMD Ryzen 5 2600X\", \"GPUName\": \"AMD Radeon RX 5700 XT\"}, \"LOADS\": {\"CPU Core #1\": 500, \"CPU Core #2\": 500, \"CPU Core #3\": 500, \"CPU Core #4\": 0, \"CPU Core #5\": 0, \"CPU Core #6\": 500, \"CPU Core #7\": 500, \"CPU Core #8\": 0, \"CPU Core #9\": 500, \"CPU Core #10\": 0, \"CPU Core #11\": 500, \"CPU Core #12\": 0, \"CPU Total\": 292, \"Memory\": 333, \"Virtual Memory\": 443, \"Fullscreen FPS\": \"1000\", \"D3D 3D\": 65, \"GPU Core\": 50, \"GPU Memory\": 30}, \"TEMPERATURES\": {\"GPU Core\": 490, \"GPU Memory\": 520, \"GPU Hot Spot\": 490}, \"CLOCKS\": {\"GPU Core\": 660, \"GPU Memory\": 4080}}")
    received = uart.reciveMessage()
    uart.close()
    print("received =", received)