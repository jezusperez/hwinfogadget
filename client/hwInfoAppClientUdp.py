from PIL import Image, ImageSequence
import pygame
from pygame.locals import *
import time
import array
import math
import json
import os
import socket
import sys
import select

from pathlib import Path
# > pip install Pillow
# > pip install pygame

def pilImageToSurface(pilImage):
    mode, size, data = pilImage.mode, pilImage.size, pilImage.tobytes()
    return pygame.image.fromstring(data, size, mode).convert_alpha()

def loadGIF(filename):
    pilImage = Image.open(filename)
    frames = []
    if pilImage.format == 'GIF' and pilImage.is_animated:
        for frame in ImageSequence.Iterator(pilImage):
            pygameImage = pilImageToSurface(frame.convert('RGBA'))
            frames.append(pygameImage)
    else:
        frames.append(pilImageToSurface(pilImage))
    return frames

def renderImage(x, y, width, height, screen, image):
    image = pygame.transform.scale(image, (width, height))
    screen.blit(image, (x, y))

def renderRotatedImage(x, y, width, height, angle, screen, image):
    image = pygame.transform.scale(image, (width, height))
    image_rect = image.get_rect(topleft = (x - (width /2), (y - (height /2))))
    offset_center_to_pivot = pygame.math.Vector2((x, y)) - image_rect.center
    rotated_offset = offset_center_to_pivot.rotate(-angle)
    rotated_image_center = (x - rotated_offset.x, y - rotated_offset.y)
    rotated_image = pygame.transform.rotate(image, angle)
    rotated_image_rect = rotated_image.get_rect(center = rotated_image_center)
    screen.blit(rotated_image, rotated_image_rect)

def keyExist(dict, key):
    if key in dict:
        print("Exists")
    else:
        print("Does not exist")

themeFolder = "cyberpunk"
def main():

    print("load: ", os.path.abspath(os.getcwd()) + "/" + "/udpConfig.json")
    with open(os.path.abspath(os.getcwd()) + "/" + "/udpConfig.json") as f:
        uartData = json.load(f)

    
    print("UART: ", uartData["port"], " | ", uartData["device"], " | ", uartData["baud"])
    uartInputPort = uartDevice(serialPortName = uartData["port"], 
            serialPortDevice = uartData["device"], 
             serialPortSpeed = int(uartData["baud"]));
    uartInputPort.open()
    print("uartInputPort = ", uartInputPort)
    
    backgrounds = os.listdir(str(Path.cwd()) + "/background/")
    backgroundIndex = 0
    # print("backgrounds =", backgrounds)

    pygame.init()
    screen = pygame.display.set_mode((800, 480))

    themeData = []
    with open(os.path.abspath(os.getcwd()) + "/" + themeFolder + "/theme.json") as f:
        themeData = json.load(f)

    # for bitmap in themeData["BITMAPS"]:
    #     print("bitmap =", bitmap)
    #     bitmap = pygame.image.load(bitmap["BACKGROUND"]).convert_alpha()
    #     print("bitmap =", bitmap)
    if "BITMAPS" in themeData:
        for name, dict_ in themeData["BITMAPS"].items():
            # print('the name of the dictionary is ', name)
            # print('the dictionary looks like ', dict_)
            themeData["BITMAPS"][name] = pygame.image.load(themeData["BITMAPS"][name]).convert_alpha()
    
    if "FONTS" in themeData:
        for name, dict_ in themeData["FONTS"].items():
            themeData["FONTS"][name] = pygame.font.Font(
                os.path.join("res", "fonts", os.path.abspath(os.getcwd()) + "/" + themeFolder + "/" + themeData["FONTS"][name]["file"]), 
                int(themeData["FONTS"][name]["size"]))
        
    # for name, dict_ in themeData["LABELS"].items():
    #     # print('the name of the dictionary is ', name)
    #     # print('the dictionary looks like ', dict_)
    #     # print("themeData[\"FONTS\"][name] =", themeData["FONTS"][name])
    #     selectedFont = themeData["LABELS"][name]["font"]
    #     # print("selectedFont =", selectedFont)
    #     # print("themeData[\"LABELS\"][name][\"size\"] =", themeData["LABELS"][name]["size"])
    #     # print("themeData[LABELS][" + name + "][size] =", themeData["LABELS"][name]["size"])
    #     # # print("font path =", os.path.join("res", "fonts", os.path.abspath(os.getcwd()) + "/" + themeData["FONTS"][name]))
    #     # # print(themeData["LABELS"][name]["size"])
    #     # # print(themeData["LABELS"][name]["color"])
    #     # # print(themeData["LABELS"][name]["font"])
    #     # # font = pygame.font.Font(os.path.join("res", "fonts", themeData["FONTS"]["default"]), themeData["LABELS"][name]["size"]
    #     themeData["LABELS"][name]["font"] = pygame.font.Font(
    #         os.path.join("res", "fonts", os.path.abspath(os.getcwd()) + "/" + themeData["FONTS"][selectedFont]), 
    #         int(themeData["LABELS"][name]["size"]))

    # hwInfo = getHwInfo()
    # # jsonObj = json.dumps(hwInfo, indent=2)
    # # print(jsonObj)

    pygame.init()
    screen = pygame.display.set_mode((800, 480))

    animatedBack = loadGIF(themeData["ANIMATED_BACKGROUND"])
    animatedBackIndex = 0
    background = pygame.image.load(themeData["BACKGROUND"]).convert_alpha()
    
    print("uartInputPort = ", uartInputPort)

    while 1:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                return
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    backgroundIndex = backgroundIndex - 1
                    if backgroundIndex < 0:
                        backgroundIndex = len(backgrounds) - 1
                        animatedBack = loadGIF(themeData["ANIMATED_BACKGROUND"])
                        animatedBackIndex = 0
                    else:
                        animatedBack = loadGIF(str(Path.cwd()) + "/background/" + backgrounds[backgroundIndex])
                        animatedBackIndex = 0
                elif event.key == pygame.K_RIGHT:
                    backgroundIndex = backgroundIndex + 1
                    if backgroundIndex >= len(backgrounds):
                        backgroundIndex = 0
                        animatedBack = loadGIF(themeData["ANIMATED_BACKGROUND"])
                        animatedBackIndex = 0
                    else:
                        animatedBack = loadGIF(str(Path.cwd()) + "/background/" + backgrounds[backgroundIndex])
                        animatedBackIndex = 0
                elif event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    return

        # hwInfo = getHwInfo()
        # # jsonObj = json.dumps(hwInfo["GPU"]["LOADS"], indent=2)
        # # print(hwInfo["GPU"]["LOADS"])

        renderImage(0, 0, screen.get_width(), screen.get_height(), screen, animatedBack[animatedBackIndex])
        renderImage(0, 0, screen.get_width(), screen.get_height(), screen, background)

        animatedBackIndex = animatedBackIndex + 1;
        if(animatedBackIndex >= len(animatedBack)):
            animatedBackIndex = 0;
        
        uartDataInput = uartInputPort.reciveMessage()
        try:
            # hwInfo = json.loads("".join(map(chr, uartDataInput)))
            hwInfo = json.loads(uartDataInput)
        except:
            hwInfo = None
        
        if hwInfo != None:
            # TEXT
            if "TEXT" in themeData:
                for name, dict_ in themeData["TEXT"].items():
                    labelSurface = themeData["FONTS"][themeData["TEXT"][name]["font"]].render(
                        themeData["TEXT"][name]["value"],
                        True, 
                        themeData["TEXT"][name]["colorShadow"])
                    renderImage(themeData["TEXT"][name]["x"] - (labelSurface.get_width() / 2) + themeData["TEXT"][name]["xShadow"], 
                                themeData["TEXT"][name]["y"] - (labelSurface.get_height() / 2) + themeData["TEXT"][name]["yShadow"], 
                                labelSurface.get_width(), labelSurface.get_height(), 
                                screen, labelSurface)
                    
                    labelSurface = themeData["FONTS"][themeData["TEXT"][name]["font"]].render(
                        themeData["TEXT"][name]["value"],
                        True, 
                        themeData["TEXT"][name]["color"])
                    renderImage(themeData["TEXT"][name]["x"] - (labelSurface.get_width() / 2), 
                                themeData["TEXT"][name]["y"] - (labelSurface.get_height() / 2), 
                                labelSurface.get_width(), labelSurface.get_height(), 
                                screen, labelSurface)

            # LABELS SHADOW
            if "LABELS_SHADOW" in themeData:
                for name, dict_ in themeData["LABELS_SHADOW"].items():
                    labelSurface = themeData["FONTS"][themeData["LABELS"][name]["font"]].render(
                        hwInfo[name], 
                        True, 
                        themeData["LABELS_SHADOW"][name]["color"])
                    renderImage(themeData["LABELS"][name]["x"] + themeData["LABELS_SHADOW"][name]["offsetX"], 
                                themeData["LABELS"][name]["y"] + themeData["LABELS_SHADOW"][name]["offsetY"], 
                                labelSurface.get_width(), labelSurface.get_height(), screen, labelSurface)
            #  LABELS
            if "LABELS" in themeData:
                for name, dict_ in themeData["LABELS"].items():
                    # print("themeData[\"LABELS\"][" + name + "][\"font\"] =", themeData["LABELS"][name]["font"])
                    labelSurface = themeData["FONTS"][themeData["LABELS"][name]["font"]].render(
                        hwInfo[name], 
                        True, 
                        themeData["LABELS"][name]["color"])
                    renderImage(themeData["LABELS"][name]["x"], 
                                themeData["LABELS"][name]["y"], 
                                labelSurface.get_width(), labelSurface.get_height(), screen, labelSurface)
            # renderRotatedImage(width / 2, height / 2, screen.get_width(), screen.get_height(), 1, screen, hulk[index])

            if "GAUGE_TYPE_A_SHADOW" in themeData:
                for name, dict_ in themeData["GAUGE_TYPE_A_SHADOW"].items():
                    if name in themeData["GAUGE_TYPE_A"]:
                        number = hwInfo["LOADS"][name]
                        number = number.split(" ")
                        number = number[0]
                        renderRotatedImage(
                            themeData["GAUGE_TYPE_A"][name]["x"] + themeData["GAUGE_TYPE_A_SHADOW"][name]["offsetX"], 
                                themeData["GAUGE_TYPE_A"][name]["y"] + themeData["GAUGE_TYPE_A_SHADOW"][name]["offsetY"], 
                            themeData["GAUGE_TYPE_A"][name]["width"], themeData["GAUGE_TYPE_A"][name]["height"], 
                            (((themeData["GAUGE_TYPE_A"][name]["maxGraph"] - themeData["GAUGE_TYPE_A"][name]["minGraph"]) /    \
                            (themeData["GAUGE_TYPE_A"][name]["realValMax"] - themeData["GAUGE_TYPE_A"][name]["realValMin"])) *  \
                            -float(number)) + int(themeData["GAUGE_TYPE_A"][name]["minGraph"]),
                            screen, 
                            themeData["BITMAPS"][themeData["GAUGE_TYPE_A_SHADOW"][name]["bitmap"]])
                        
            if "GAUGE_TYPE_A" in themeData:
                for name, dict_ in themeData["GAUGE_TYPE_A"].items():
                    number = hwInfo["LOADS"][name]
                    number = number.split(" ")
                    number = number[0]
                    renderRotatedImage(
                        themeData["GAUGE_TYPE_A"][name]["x"], themeData["GAUGE_TYPE_A"][name]["y"], 
                        themeData["GAUGE_TYPE_A"][name]["width"], themeData["GAUGE_TYPE_A"][name]["height"], 
                        (((themeData["GAUGE_TYPE_A"][name]["maxGraph"] - themeData["GAUGE_TYPE_A"][name]["minGraph"]) /    \
                        (themeData["GAUGE_TYPE_A"][name]["realValMax"] - themeData["GAUGE_TYPE_A"][name]["realValMin"])) *  \
                        -float(number)) + int(themeData["GAUGE_TYPE_A"][name]["minGraph"]),
                        screen, 
                        themeData["BITMAPS"][themeData["GAUGE_TYPE_A"][name]["bitmap"]])
            
            if "LOADS_SHADOW" in themeData:
                for name, dict_ in themeData["LOADS_SHADOW"].items():
                    if name in themeData["LOADS"]:
                        number = hwInfo["LOADS"][name]
                        number = number.split(" ")
                        number = number[0]
                        renderRotatedImage(
                            themeData["LOADS"][name]["x"] + themeData["LOADS_SHADOW"][name]["offsetX"], 
                            themeData["LOADS"][name]["y"] + themeData["LOADS_SHADOW"][name]["offsetY"], 
                            themeData["LOADS"][name]["width"], themeData["LOADS"][name]["height"], 
                            (((themeData["LOADS"][name]["maxGraph"] - themeData["LOADS"][name]["minGraph"]) /    \
                            (themeData["LOADS"][name]["realValMax"] - themeData["LOADS"][name]["realValMin"])) *  \
                            -float(number)) + int(themeData["LOADS"][name]["minGraph"]),
                            screen, 
                            themeData["BITMAPS"][themeData["LOADS_SHADOW"][name]["bitmap"]])

            if "LOADS" in themeData:
                for name, dict_ in themeData["LOADS"].items():
                    number = hwInfo["LOADS"][name]
                    number = number.split(" ")
                    number = number[0]
                    renderRotatedImage(
                        themeData["LOADS"][name]["x"], themeData["LOADS"][name]["y"], 
                        themeData["LOADS"][name]["width"], themeData["LOADS"][name]["height"], 
                        (((themeData["LOADS"][name]["maxGraph"] - themeData["LOADS"][name]["minGraph"]) /    \
                        (themeData["LOADS"][name]["realValMax"] - themeData["LOADS"][name]["realValMin"])) *  \
                        -float(number)) + int(themeData["LOADS"][name]["minGraph"]),
                        screen, 
                    themeData["BITMAPS"][themeData["LOADS"][name]["bitmap"]])

            # TEMPERATURES
            if "TEMPERATURES" in themeData:
                for name, dict_ in themeData["TEMPERATURES"].items():
                    number = hwInfo["TEMPERATURES"][name]
                    number = number.split(" ")
                    number = int(float(number[0]))
                    if number >= themeData["TEMPERATURES"][name]["realValueMax"]:
                        number = themeData["TEMPERATURES"][name]["realValueMax"]
                    # themeData["TEMPERATURES"][name]["value"] = number
                    # number = ((themeData["TEMPERATURES"][name]["realValueMax"] - themeData["TEMPERATURES"][name]["realValueMin"]) / 131) * themeData["TEMPERATURES"][name]["value"]
                    number = ((themeData["TEMPERATURES"][name]["realValueMax"] - \
                            themeData["TEMPERATURES"][name]["realValueMin"]) / \
                            themeData["TEMPERATURES"][name]["height"]) * \
                            number
                    renderImage(
                        themeData["TEMPERATURES"][name]["x"], 
                        themeData["TEMPERATURES"][name]["y"] - number, 
                        themeData["TEMPERATURES"][name]["width"], 
                        number, 
                        screen, 
                        themeData["BITMAPS"][themeData["TEMPERATURES"][name]["bitmap"]])
                    renderImage(
                        themeData["TEMPERATURES"][name]["x"], 
                        themeData["TEMPERATURES"][name]["y"] - number - themeData["BITMAPS"][themeData["TEMPERATURES"][name]["bitmapTop"]].get_height(), 
                        themeData["BITMAPS"][themeData["TEMPERATURES"][name]["bitmapTop"]].get_width(), 
                        themeData["BITMAPS"][themeData["TEMPERATURES"][name]["bitmapTop"]].get_height(), 
                        screen, 
                        themeData["BITMAPS"][themeData["TEMPERATURES"][name]["bitmapTop"]])
            # lOADS CPU
            if "GAUGE_TYPE_D" in themeData:
                for name, dict_ in themeData["GAUGE_TYPE_D"].items():
                    number = hwInfo["LOADS"][name]
                    number = number.split(" ")
                    number = int(float(number[0]))
                    if number >= themeData["GAUGE_TYPE_D"][name]["realValueMax"]:
                        number = themeData["GAUGE_TYPE_D"][name]["realValueMax"]
                    themeData["GAUGE_TYPE_D"][name]["value"] = number
                    number = ((themeData["GAUGE_TYPE_D"][name]["realValueMax"] - themeData["GAUGE_TYPE_D"][name]["realValueMin"]) / \
                            themeData["GAUGE_TYPE_D"][name]["width"] \
                            ) * themeData["GAUGE_TYPE_D"][name]["value"]
                    # if number > themeData["GAUGE_TYPE_D"][name]["width"]:
                    #     number = themeData["GAUGE_TYPE_D"][name]["width"]
                    renderImage(
                        themeData["GAUGE_TYPE_D"][name]["x"], 
                        themeData["GAUGE_TYPE_D"][name]["y"], 
                        number, 
                        themeData["GAUGE_TYPE_D"][name]["height"], 
                        screen, 
                        themeData["BITMAPS"][themeData["GAUGE_TYPE_D"][name]["bitmap"]])
                    renderImage(
                        themeData["GAUGE_TYPE_D"][name]["x"] + number, 
                        themeData["GAUGE_TYPE_D"][name]["y"], 
                        themeData["BITMAPS"][themeData["GAUGE_TYPE_D"][name]["bitmapRight"]].get_width(), 
                        themeData["GAUGE_TYPE_D"][name]["height"], # themeData["BITMAPS"][themeData["GAUGE_TYPE_D"][name]["bitmapRight"]].get_height(), 
                        screen, 
                        themeData["BITMAPS"][themeData["GAUGE_TYPE_D"][name]["bitmapRight"]])

            if "Fullscreen FPS" in themeData:
                if "Fullscreen FPS" in hwInfo["LOADS"]:
                    number = hwInfo["LOADS"]["Fullscreen FPS"]
                    number = int(float(number))
                    if number > 0:
                        renderImage((screen.get_width() / 2) - (themeData["BITMAPS"][themeData["Fullscreen FPS"]["bitmap"]].get_width() / 2), 
                                    (screen.get_height() / 2) - (themeData["BITMAPS"][themeData["Fullscreen FPS"]["bitmap"]].get_height() / 2), 
                                    themeData["BITMAPS"][themeData["Fullscreen FPS"]["bitmap"]].get_width(), 
                                    themeData["BITMAPS"][themeData["Fullscreen FPS"]["bitmap"]].get_height(), 
                                    screen, themeData["BITMAPS"][themeData["Fullscreen FPS"]["bitmap"]])
                        labelSurface = themeData["FONTS"][themeData["Fullscreen FPS"]["font"]].render(
                            themeData["Fullscreen FPS"]["textPrev"] + str(number), 
                            True, 
                            themeData["Fullscreen FPS"]["fontColor"])
                        renderImage((screen.get_width() / 2) - (labelSurface.get_width() / 2), 
                                    (screen.get_height() / 2) - (labelSurface.get_height() / 2), 
                                    labelSurface.get_width(), labelSurface.get_height(), screen, labelSurface)
            
        pygame.display.flip()

        # time.sleep(0.10)

if __name__ == "__main__":
    main()
    
