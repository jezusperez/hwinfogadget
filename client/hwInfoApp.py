from PIL import Image, ImageSequence
import pygame
from pygame.locals import *
import pygame.gfxdraw
import time
import array
import math
import json
import os
import socket
import sys
import select
import copy
import random

from pathlib import Path
from systeminfo import getHwInfo
# > pip install Pillow
# > pip install pygame

ENABLE_LOG = True
def logMessage(*args):
    if ENABLE_LOG is True:
        for arg in args:
            print("hwInfoApp Module:", arg)

def gethardwareInfo():
    hwInfo = getHwInfo()
    # jsonObj = json.dumps(hwInfo, indent=2)
    # print(jsonObj)
    jsonHwInfo = json.dumps(hwInfo)
    # logMessage("jsonHwInfo", jsonHwInfo)
    # jsonHwInfo = jsonHwInfo.replace(' %', '')
    # jsonHwInfo = jsonHwInfo.replace(' \\u00b0C', '')
    # jsonHwInfo = jsonHwInfo.replace(' MHz', '')

    return json.loads(jsonHwInfo)
#             jsonHwInfo = bytes(jsonHwInfo, 'utf-8')
# #             # jsonHwInfo = jsonHwInfo.replace('CPU Core ', 'CPU')
# #             # jsonHwInfo = jsonHwInfo.replace('CPU Total', 'CPU')
# #             # jsonHwInfo = jsonHwInfo.replace('GPU ', '')
# #             # jsonHwInfo = jsonHwInfo.replace(' Memory', '')
# #             # jsonHwInfo = jsonHwInfo.replace('Fullscreen ', '')
# #             # jsonHwInfo = jsonHwInfo.replace('Memory', 'Mem')
# #             # # jsonHwInfo = jsonHwInfo.replace('TEMPERATURES', 'TEMP')
# #             # jsonHwInfo = jsonHwInfo.replace('D3D 3D', 'D3D')
# #             # jsonHwInfo = jsonHwInfo.replace('Name', '')
# #             logMessage("jsonHwInfo", jsonHwInfo)
# #             # logMessage("runThread loop")
# #             jsonData = json.loads(jsonHwInfo)
# #             print(len(json.dumps(jsonData["HW"])))
# #             print(json.dumps(jsonData["LOADS"]))
# #             print(len(json.dumps(jsonData["TEMPERATURES"])))
# #             print(len(json.dumps(jsonData["CLOCKS"])))
# #             outputData = []
# #             outputData.append(jsonData["HW"]["CPUName"])
# #             outputData.append(jsonData["HW"]["GPUName"])
# #             outputData.append(jsonData["LOADS"]["CPU Core #1"])
# #             outputData.append(jsonData["LOADS"]["CPU Core #2"])
# #             outputData.append(jsonData["LOADS"]["CPU Core #3"])
# #             outputData.append(jsonData["LOADS"]["CPU Core #4"])
# #             outputData.append(jsonData["LOADS"]["CPU Core #5"])
# #             outputData.append(jsonData["LOADS"]["CPU Core #6"])
# # "CPU Core #7"])
# # "CPU Core #8"])
# # "CPU Core #9"])
# # "CPU Core #10"])
# # "CPU Core #11"])
# # "CPU Core #12"])
# # "CPU Total"])
# # "Memory"])
# # "Virtual Memory"])
# # "Fullscreen FPS"])
# # "D3D 3D"])
# # "GPU Core"])
# # "GPU Memory"])
#             # logMessage(json.dumps({"PAYLOAD":outputData}))
#             # logMessage(len(json.dumps(jsonData["HW"])))
def setMappedValue(value, targetValue):
    if value < targetValue:
        value = value + 1
    elif value > targetValue:
        value = value - 1
    return value

def radians(degs):
    return degs * 3.141593 / 180.0

def mapValue(minOutputValue, maxOutputValue, minInputValues, maxInputValues, actualValue):
    return minOutputValue + (((maxOutputValue - minOutputValue) / (maxInputValues - minInputValues)) * actualValue)

def pilImageToSurface(pilImage):
    mode, size, data = pilImage.mode, pilImage.size, pilImage.tobytes()
    return pygame.image.fromstring(data, size, mode).convert_alpha()

def loadGIF(filename):
    pilImage = Image.open(filename)
    frames = []
    if pilImage.format == 'GIF' and pilImage.is_animated:
        for frame in ImageSequence.Iterator(pilImage):
            pygameImage = pilImageToSurface(frame.convert('RGBA'))
            frames.append(pygameImage)
    else:
        frames.append(pilImageToSurface(pilImage))
    return frames

def renderImage(x, y, width, height, screen, image):
    image = pygame.transform.scale(image, (width, height))
    screen.blit(image, (x, y))

def renderRotatedImage(x, y, width, height, angle, screen, image):
    image = pygame.transform.scale(image, (width, height))
    image_rect = image.get_rect(topleft = (x - (width /2), (y - (height /2))))
    offset_center_to_pivot = pygame.math.Vector2((x, y)) - image_rect.center
    rotated_offset = offset_center_to_pivot.rotate(-angle)
    rotated_image_center = (x - rotated_offset.x, y - rotated_offset.y)
    rotated_image = pygame.transform.rotate(image, angle)
    rotated_image_rect = rotated_image.get_rect(center = rotated_image_center)
    screen.blit(rotated_image, rotated_image_rect)

def keyExist(dict, key):
    if key in dict:
        return True
    else:
        return False

def createLocalFont(fontData, fontFolder):
    temporalryFont = pygame.font.Font(
        os.path.join("res", "fonts", fontFolder + fontData["file"]), 
        fontData["size"])
    # print()
    return temporalryFont
# gaugeDefaultFontDefinition = {"file":"Perfect_Futures.ttf", "size": 56}
# gaugeDefaultFontSmallDefinition = {"file":"wheaton_capitals.otf", "size": 24}
# barDefaultLabelFontDefinition = {"file":"wheaton_capitals.otf", "size": 28}
# barDefaultValueFontDefinition = {"file":"wheaton_capitals.otf", "size": 20}
# gaugeDefaultFont = None
# gaugeDefaultFontSmall = None
# barDefaultLabelFont = None
# barDefaultValueFont = None

themeFonts = {
    "themeFontSize102": {"file":"AtariST8x16SystemFont.ttf", "size": 102},
    "themeFontSize96": {"file":"AtariST8x16SystemFont.ttf", "size": 96},
    "themeFontSize88": {"file":"AtariST8x16SystemFont.ttf", "size": 88},
    "themeFontSize80": {"file":"AtariST8x16SystemFont.ttf", "size": 80},
    "themeFontSize72": {"file":"AtariST8x16SystemFont.ttf", "size": 72},
    "themeFontSize64": {"file":"AtariST8x16SystemFont.ttf", "size": 64},
    "themeFontSize56": {"file":"AtariST8x16SystemFont.ttf", "size": 56},
    "themeFontSize48": {"file":"AtariST8x16SystemFont.ttf", "size": 48},
    "themeFontSize40": {"file":"AtariST8x16SystemFont.ttf", "size": 40},
    "themeFontSize32": {"file":"AtariST8x16SystemFont.ttf", "size": 32},
    "themeFontSize24": {"file":"AtariST8x16SystemFont.ttf", "size": 24},
    "themeFontSize16": {"file":"AtariST8x16SystemFont.ttf", "size": 16},
    "themeFontSize12":  {"file":"AtariST8x16SystemFont.ttf", "size": 12},
    "themeFontSize8":  {"file":"AtariST8x16SystemFont.ttf", "size": 8},
    # digital-7-(mono).ttf
    "digital-7102": {"file":"digital-7-(mono).ttf", "size": 102},
    "digital-796": {"file":"digital-7-(mono).ttf", "size": 96},
    "digital-788": {"file":"digital-7-(mono).ttf", "size": 88},
    "digital-780": {"file":"digital-7-(mono).ttf", "size": 80},
    "digital-772": {"file":"digital-7-(mono).ttf", "size": 72},
    "digital-764": {"file":"digital-7-(mono).ttf", "size": 64},
    "digital-756": {"file":"digital-7-(mono).ttf", "size": 56},
    "digital-748": {"file":"digital-7-(mono).ttf", "size": 48},
    "digital-740": {"file":"digital-7-(mono).ttf", "size": 40},
    "digital-732": {"file":"digital-7-(mono).ttf", "size": 32},
    "digital-724": {"file":"digital-7-(mono).ttf", "size": 24},
    "digital-716": {"file":"digital-7-(mono).ttf", "size": 16},
    "digital-712":  {"file":"digital-7-(mono).ttf", "size": 12},
    "digital-78":  {"file":"digital-7-(mono).ttf", "size": 8},
    # The2K12.ttf
    "The2K12126": {"file":"The2K12.ttf", "size": 126},
    "The2K12118": {"file":"The2K12.ttf", "size": 118},
    "The2K12110": {"file":"The2K12.ttf", "size": 110},
    "The2K12102": {"file":"The2K12.ttf", "size": 102},
    "The2K1296": {"file":"The2K12.ttf", "size": 96},
    "The2K1288": {"file":"The2K12.ttf", "size": 88},
    "The2K1296": {"file":"The2K12.ttf", "size": 96},
    "The2K1288": {"file":"The2K12.ttf", "size": 88},
    "The2K1280": {"file":"The2K12.ttf", "size": 80},
    "The2K1272": {"file":"The2K12.ttf", "size": 72},
    "The2K1264": {"file":"The2K12.ttf", "size": 64},
    "The2K1256": {"file":"The2K12.ttf", "size": 56},
    "The2K1248": {"file":"The2K12.ttf", "size": 48},
    "The2K1240": {"file":"The2K12.ttf", "size": 40},
    "The2K1232": {"file":"The2K12.ttf", "size": 32},
    "The2K1224": {"file":"The2K12.ttf", "size": 24},
    "The2K1216": {"file":"The2K12.ttf", "size": 16},
    "The2K1212":  {"file":"The2K12.ttf", "size": 12},
    "The2K128":  {"file":"The2K12.ttf", "size": 8},
    # ATOMICCLOCKRADIO.TTF
    "ATOMICCLOCKRADIO102": {"file":"ATOMICCLOCKRADIO.ttf", "size": 102},
    "ATOMICCLOCKRADIO96": {"file":"ATOMICCLOCKRADIO.ttf", "size": 96},
    "ATOMICCLOCKRADIO88": {"file":"ATOMICCLOCKRADIO.ttf", "size": 88},
    "ATOMICCLOCKRADIO96": {"file":"ATOMICCLOCKRADIO.ttf", "size": 96},
    "ATOMICCLOCKRADIO88": {"file":"ATOMICCLOCKRADIO.ttf", "size": 88},
    "ATOMICCLOCKRADIO80": {"file":"ATOMICCLOCKRADIO.TTF", "size": 80},
    "ATOMICCLOCKRADIO72": {"file":"ATOMICCLOCKRADIO.TTF", "size": 72},
    "ATOMICCLOCKRADIO64": {"file":"ATOMICCLOCKRADIO.TTF", "size": 64},
    "ATOMICCLOCKRADIO56": {"file":"ATOMICCLOCKRADIO.TTF", "size": 56},
    "ATOMICCLOCKRADIO48": {"file":"ATOMICCLOCKRADIO.TTF", "size": 48},
    "ATOMICCLOCKRADIO40": {"file":"ATOMICCLOCKRADIO.TTF", "size": 40},
    "ATOMICCLOCKRADIO32": {"file":"ATOMICCLOCKRADIO.TTF", "size": 32},
    "ATOMICCLOCKRADIO24": {"file":"ATOMICCLOCKRADIO.TTF", "size": 24},
    "ATOMICCLOCKRADIO16": {"file":"ATOMICCLOCKRADIO.TTF", "size": 16},
    "ATOMICCLOCKRADIO12":  {"file":"ATOMICCLOCKRADIO.TTF", "size": 12},
    "ATOMICCLOCKRADIO8":  {"file":"ATOMICCLOCKRADIO.TTF", "size": 8},
    # Coalition_v2.ttf
    "Coalition102": {"file":"Coalition_v2.ttf", "size": 102},
    "Coalition96": {"file":"Coalition_v2.ttf", "size": 96},
    "Coalition88": {"file":"Coalition_v2.ttf", "size": 88},
    "Coalition80": {"file":"Coalition_v2.ttf", "size": 80},
    "Coalition72": {"file":"Coalition_v2.ttf", "size": 72},
    "Coalition64": {"file":"Coalition_v2.ttf", "size": 64},
    "Coalition56": {"file":"Coalition_v2.ttf", "size": 56},
    "Coalition48": {"file":"Coalition_v2.ttf", "size": 48},
    "Coalition40": {"file":"Coalition_v2.ttf", "size": 40},
    "Coalition32": {"file":"Coalition_v2.ttf", "size": 32},
    "Coalition24": {"file":"Coalition_v2.ttf", "size": 24},
    "Coalition16": {"file":"Coalition_v2.ttf", "size": 16},
    "Coalition12":  {"file":"Coalition_v2.ttf", "size": 12},
    "Coalition8":  {"file":"Coalition_v2.ttf", "size": 8},
    # RV5PRD___.ttf
    "V5PRD___102": {"file":"V5PRD___.ttf", "size": 102},
    "V5PRD___96": {"file":"V5PRD___.ttf", "size": 96},
    "V5PRD___88": {"file":"V5PRD___.ttf", "size": 88},
    "V5PRD___80": {"file":"V5PRD___.ttf", "size": 80},
    "V5PRD___72": {"file":"V5PRD___.ttf", "size": 72},
    "V5PRD___64": {"file":"V5PRD___.ttf", "size": 64},
    "V5PRD___56": {"file":"V5PRD___.ttf", "size": 56},
    "V5PRD___48": {"file":"V5PRD___.ttf", "size": 48},
    "V5PRD___40": {"file":"V5PRD___.ttf", "size": 40},
    "V5PRD___32": {"file":"V5PRD___.ttf", "size": 32},
    "V5PRD___24": {"file":"V5PRD___.ttf", "size": 24},
    "V5PRD___16": {"file":"V5PRD___.ttf", "size": 16},
    "V5PRD___12":  {"file":"V5PRD___.ttf", "size": 12},
    "V5PRD___8":  {"file":"V5PRD___.ttf", "size": 8},
    # 16Segments-Basic.otf
    "16SegmentsBasic102": {"file":"16Segments-Basic.otf", "size": 102},
    "16SegmentsBasic96": {"file":"16Segments-Basic.otf", "size": 96},
    "16SegmentsBasic88": {"file":"16Segments-Basic.otf", "size": 88},
    "16SegmentsBasic80": {"file":"16Segments-Basic.otf", "size": 80},
    "16SegmentsBasic72": {"file":"16Segments-Basic.otf", "size": 72},
    "16SegmentsBasic64": {"file":"16Segments-Basic.otf", "size": 64},
    "16SegmentsBasic56": {"file":"16Segments-Basic.otf", "size": 56},
    "16SegmentsBasic48": {"file":"16Segments-Basic.otf", "size": 48},
    "16SegmentsBasic40": {"file":"16Segments-Basic.otf", "size": 40},
    "16SegmentsBasic32": {"file":"16Segments-Basic.otf", "size": 32},
    "16SegmentsBasic24": {"file":"16Segments-Basic.otf", "size": 24},
    "16SegmentsBasic16": {"file":"16Segments-Basic.otf", "size": 16},
    "16SegmentsBasic12":  {"file":"16Segments-Basic.otf", "size": 12},
    "16SegmentsBasic8":  {"file":"16Segments-Basic.otf", "size": 8}
}
# gaugeDefaultFont = createFont(gaugeDefaultFont) NEED TO BE AFTER PYGAME INITIALIZATION

gaugeObject = {
    "x": 100,
    "y": 100,
    "size": 200,
    "barWidth": 32,
    "lineWidth": 8,
    "colorGauge": Color(0x80, 0x80, 0x80),
    "colorGaugeValue": Color(0xff, 0xff, 0xff),
    "colorLabel": Color(0x80, 0x80, 0x80),
    "colorValue": Color(0xff, 0xff, 0xff),
    "value": 50,
    "valueTarget": 0,
    "label": "MHz",
    "enableLabel": True,
    "enableValue": True, 
    "Type": "gaugeObject"
}
def drawGauge(target, gaugeObject, styleObject, colorObject, fontObject):
    targetSurface = pygame.Surface(
        (gaugeObject["size"], gaugeObject["size"]), 
        pygame.SRCALPHA)
    # for i in range(0, width - 1):
    ## Angle Low = 45
    ## Angle Hi = 315
    ## Angle apperture 315 - 45 = 270
    for i in range(45, 315):
        pygame.draw.arc(targetSurface, 
            colorObject[styleObject["colorGauge"]], 
            [0, 0, gaugeObject["size"], gaugeObject["size"]], 
            radians(i), 
            radians(i + 0.5), 
            gaugeObject["lineWidth"])
        pygame.draw.arc(targetSurface, 
            colorObject[styleObject["colorGauge"]], 
            [0, 0, gaugeObject["size"], gaugeObject["size"]], 
            radians(i - 0.5), 
            radians(i), 
            gaugeObject["lineWidth"])
        pygame.draw.arc(targetSurface, 
            colorObject[styleObject["colorGauge"]], 
            [0, 0, gaugeObject["size"], gaugeObject["size"]], 
            radians(i), 
            radians(i + 0.25), 
            gaugeObject["lineWidth"])
        pygame.draw.arc(targetSurface, 
            colorObject[styleObject["colorGauge"]], 
            [0, 0, gaugeObject["size"], gaugeObject["size"]], 
            radians(i - 0.25), 
            radians(i), 
            gaugeObject["lineWidth"])
        # pygame.draw.arc(targetSurface, 
        #     colorGaugeValue, 
        #     [0, 0, size, size], 
        #     radians(i), 
        #     radians(i + 0.5), 
        #     width)
    for i in range(315, 315 - int(mapValue(0, 270, 0, 100, gaugeObject["value"])), -1):
        pygame.draw.arc(targetSurface, 
            colorObject[styleObject["colorGaugeValue"]], 
            [0, 0, gaugeObject["size"], gaugeObject["size"]], 
            radians(i), 
            radians(i + 0.5), 
            gaugeObject["barWidth"])
        pygame.draw.arc(targetSurface, 
            colorObject[styleObject["colorGaugeValue"]], 
            [0, 0, gaugeObject["size"], gaugeObject["size"]], 
            radians(i - 0.5), 
            radians(i), 
            gaugeObject["barWidth"])
        pygame.draw.arc(targetSurface, 
            colorObject[styleObject["colorGaugeValue"]], 
            [0, 0, gaugeObject["size"], gaugeObject["size"]], 
            radians(i), 
            radians(i + 0.25), 
            gaugeObject["barWidth"])
        pygame.draw.arc(targetSurface, 
            colorObject[styleObject["colorGaugeValue"]], 
            [0, 0, gaugeObject["size"], gaugeObject["size"]], 
            radians(i - 0.25), 
            radians(i), 
            gaugeObject["barWidth"])
    for i in range(0, gaugeObject["lineWidth"] + 1):
        pygame.draw.arc(targetSurface, 
            colorObject[styleObject["colorGauge"]], 
            [0, 0, gaugeObject["size"], gaugeObject["size"]], 
            radians(45 + i), 
            radians(45 + i + 0.5), 
            gaugeObject["barWidth"])
        pygame.draw.arc(targetSurface, 
            colorObject[styleObject["colorGaugeValue"]], 
            [0, 0, gaugeObject["size"], gaugeObject["size"]], 
            radians(315 - i - 0.5), 
            radians(315 - i), 
            gaugeObject["barWidth"])
        pygame.draw.arc(targetSurface, 
            colorObject[styleObject["colorGauge"]], 
            [0, 0, gaugeObject["size"], gaugeObject["size"]], 
            radians(45 + i), 
            radians(45 + i + 0.25), 
            gaugeObject["barWidth"])
        pygame.draw.arc(targetSurface, 
            colorObject[styleObject["colorGaugeValue"]], 
            [0, 0, gaugeObject["size"], gaugeObject["size"]], 
            radians(315 - i - 0.25), 
            radians(315 - i), 
            gaugeObject["barWidth"])
        pygame.draw.arc(targetSurface, 
            colorObject[styleObject["colorGauge"]], 
            [0, 0, gaugeObject["size"], gaugeObject["size"]], 
            radians(45 + i), 
            radians(45 + i + 0.75), 
            gaugeObject["barWidth"])
        pygame.draw.arc(targetSurface, 
            colorObject[styleObject["colorGaugeValue"]], 
            [0, 0, gaugeObject["size"], gaugeObject["size"]], 
            radians(315 - i - 0.75), 
            radians(315 - i), 
            gaugeObject["barWidth"])
    renderRotatedImage(gaugeObject["x"] + (gaugeObject["size"] / 2), gaugeObject["y"] + (gaugeObject["size"] / 2), 
        gaugeObject["size"], gaugeObject["size"], 
        -90, 
        target, targetSurface)
    # print(type(gaugeObject["font"]))
    # print(os.path.abspath(os.getcwd()) + "/" + themeFolder + "/" + gaugeObject["font"]["file"])
    if gaugeObject["enableValue"] == True:
        if colorObject["shadowOffset"] > 0:
            labelSurface = fontObject[styleObject["fontLabel"]].render(
                str(gaugeObject["value"]),
                True, 
                colorObject[styleObject["shadowColor"]])
            x = gaugeObject["x"] + (gaugeObject["size"] / 2) - (labelSurface.get_width() / 2) + colorObject["shadowOffset"]
            y = gaugeObject["y"] + (gaugeObject["size"] / 2) - (labelSurface.get_height() * 0.65) + colorObject["shadowOffset"]
            target.blit(labelSurface, 
                (x, y))
        labelSurface = fontObject[styleObject["fontLabel"]].render(
            str(gaugeObject["value"]),
            True, 
            colorObject[styleObject["colorValue"]])
        x = gaugeObject["x"] + (gaugeObject["size"] / 2) - (labelSurface.get_width() / 2)
        y = gaugeObject["y"] + (gaugeObject["size"] / 2) - (labelSurface.get_height() * 0.65)
        target.blit(labelSurface, 
            (x, y)) #(gaugeObject["x"], gaugeObject["y"]))
    if gaugeObject["enableLabel"] == True:
        if colorObject["shadowOffset"] > 0:
            labelSurface = fontObject[styleObject["fontValue"]].render(
                str(gaugeObject["label"]),
                True, 
                colorObject[styleObject["shadowColor"]])
            x = gaugeObject["x"] + (gaugeObject["size"] / 2) - (labelSurface.get_width() / 2) + colorObject["shadowOffset"]
            y = gaugeObject["y"] + gaugeObject["size"] - (labelSurface.get_height() * 1.15) + colorObject["shadowOffset"]
            target.blit(labelSurface, 
                (x, y))
        labelSurface = fontObject[styleObject["fontValue"]].render(
            str(gaugeObject["label"]),
            True, 
            colorObject[styleObject["colorValue"]])
        x = gaugeObject["x"] + (gaugeObject["size"] / 2) - (labelSurface.get_width() / 2)
        y = gaugeObject["y"] + gaugeObject["size"] - (labelSurface.get_height() * 1.15)
        target.blit(labelSurface, 
            (x, y)) #(gaugeObject["x"], gaugeObject["y"]))

genericBarObject = {
    "x": 250,
    "y": 10,
    "width": 24,
    "height": 100,
    "lineWidth": 2,
    "colorLine": Color(0xff, 0xff, 0xff),
    "colorValue": Color(0xff, 0xff, 0xff),
    "colorBack": Color(0x80, 0x80, 0x80),
    "colorLabel": Color(0xff, 0xff, 0xff),
    "value": 35,
    "valueTarget": 0,
    "label": "CPU",
    "enableLabel": True,
    "enableValue": True, 
    "Type": "verticalBarObject"
}
def drawVerticalBar(target, vBarObject, styleObject, colorObject, fontObject):
    if vBarObject["enableLabel"]:
        if colorObject["shadowOffset"] > 0:
            labelSurface = fontObject[styleObject["fontLabel"]].render(
                str(vBarObject["label"]),
                True, 
                colorObject["shadowColor"])
            x = vBarObject["x"] + (vBarObject["width"] / 2) - (labelSurface.get_width() / 2)
            y = vBarObject["y"]
            target.blit(labelSurface, 
                (x + colorObject["shadowOffset"], y + colorObject["shadowOffset"])) 
        labelSurface = fontObject[styleObject["fontLabel"]].render(
            str(vBarObject["label"]),
            True, 
            colorObject[styleObject["colorLabel"]])
        x = vBarObject["x"] + (vBarObject["width"] / 2) - (labelSurface.get_width() / 2)
        y = vBarObject["y"] # + (vBarObject["size"] / 2) - (labelSurface.get_height() / 2)
        target.blit(labelSurface, 
        (x, y)) 
    #### #### #### ####
    #### #### #### ####
    #### #### #### ####
    if vBarObject["enableLabel"]:
        realY = vBarObject["y"] + labelSurface.get_height()
    else:
        realY = vBarObject["y"]
    for i in range(0, vBarObject["lineWidth"]):
        pygame.draw.rect(target, 
            colorObject[styleObject["colorLine"]], 
            [vBarObject["x"] + i, realY + i, 
                vBarObject["width"] - (i * 2), vBarObject["height"] - (i * 2)], 
            True)
    realHeight = vBarObject["height"] - (vBarObject["lineWidth"] * 4)
    realHeight = mapValue(0, realHeight, 0, 100, vBarObject["value"])
    if realHeight > 100:
        realHeight = 100
    if realHeight < vBarObject["lineWidth"]:
        realHeight = vBarObject["lineWidth"]

    heightOffset = (vBarObject["height"] - (vBarObject["lineWidth"] * 4)) - realHeight
    pygame.draw.rect(target, 
            colorObject[styleObject["colorValue"]], 
            [vBarObject["x"] + (vBarObject["lineWidth"] * 2), realY + (vBarObject["lineWidth"] * 2) + heightOffset, 
                vBarObject["width"] - (vBarObject["lineWidth"] * 4), realHeight], 
            False) 
    #### #### #### ####
    #### #### #### ####
    #### #### #### ####
    if vBarObject["enableValue"]:
        realY = realY = vBarObject["y"] + labelSurface.get_height() + vBarObject["height"]
        if colorObject["shadowOffset"] > 0:
            labelSurface = fontObject[styleObject["fontValue"]].render(
                str(vBarObject["value"]),
                True, 
                colorObject["shadowColor"])
            x = vBarObject["x"] + (vBarObject["width"] / 2) - (labelSurface.get_width() / 2)
            # y = vBarObject["y"] # + (vBarObject["size"] / 2) - (labelSurface.get_height() / 2)
            target.blit(labelSurface, 
                (x + colorObject["shadowOffset"], realY + colorObject["shadowOffset"]))
        labelSurface = fontObject[styleObject["fontValue"]].render(
            str(vBarObject["value"]),
            True, 
            colorObject[vBarObject["colorValue"]])
        x = vBarObject["x"] + (vBarObject["width"] / 2) - (labelSurface.get_width() / 2)
        # y = vBarObject["y"] # + (vBarObject["size"] / 2) - (labelSurface.get_height() / 2)
        target.blit(labelSurface, 
            (x, realY)) 

def drawHorizontalBar(target, hBarObject, styleObject, colorObject, fontObject):
    if hBarObject["enableLabel"]:
        if colorObject["shadowOffset"] > 0:
            labelSurface = fontObject[styleObject["fontLabel"]].render(
                str(hBarObject["label"]),
                True, 
                colorObject["shadowColor"])
            x = colorObject["shadowOffset"] + hBarObject["x"]
            y = colorObject["shadowOffset"] + hBarObject["y"] + (hBarObject["height"] / 2) - (labelSurface.get_height() / 2)
            target.blit(labelSurface, 
                (x, 
                 y)) 
        labelSurface = fontObject[styleObject["fontLabel"]].render(
            str(hBarObject["label"]),
            True, 
            colorObject[styleObject["colorLabel"]])
        x = hBarObject["x"] # + (hBarObject["width"] / 2) - (labelSurface.get_width() / 2)
        y = hBarObject["y"] + (hBarObject["height"] / 2) - (labelSurface.get_height() / 2)
        target.blit(labelSurface, 
            (x, y)) 
    #### #### #### ####
    #### #### #### ####
    #### #### #### ####
    if hBarObject["enableLabel"]:
        realX = labelSurface.get_width()
    else:
        realX = 0
    for i in range(0, hBarObject["lineWidth"]):
        pygame.draw.rect(target, 
            colorObject[styleObject["colorLine"]], 
            [realX + hBarObject["x"] + i, hBarObject["y"] + i, 
                hBarObject["width"] - (i * 2), hBarObject["height"] - (i * 2)], 
            True)
    realWidth = hBarObject["width"] - (hBarObject["lineWidth"] * 4)
    realWidth = mapValue(0, realWidth, 0, 100, hBarObject["value"])
    if realWidth > 100:
        realWidth = 100
    if realWidth < hBarObject["lineWidth"]:
        realWidth = hBarObject["lineWidth"]
    # widthOffset = (hBarObject["width"] - (hBarObject["lineWidth"] * 4)) - realWidth
    pygame.draw.rect(target, 
            colorObject[styleObject["colorValue"]], 
            [realX + hBarObject["x"] + (hBarObject["lineWidth"] * 2), hBarObject["y"] + (hBarObject["lineWidth"] * 2), 
                realWidth, hBarObject["height"] - (hBarObject["lineWidth"] * 4)], 
            False)
    #### #### #### ####
    #### #### #### ####
    #### #### #### ####
    if hBarObject["enableValue"]:
        labelSurface = fontObject[styleObject["fontValue"]].render(
            str(hBarObject["value"]),
            True, 
            colorObject[styleObject["colorValue"]])
        x = hBarObject["x"] + hBarObject["width"] + realX
        y = hBarObject["y"] + (hBarObject["height"] / 2) - (labelSurface.get_height() / 2)
        target.blit(labelSurface, 
            (x, y)) 
    
def drawLabel(target, labelObject, styleObject, colorObject, fontObject):
    if colorObject["shadowOffset"] > 0:
        labelSurface = fontObject[styleObject["fontLabel"]].render(
            str(labelObject["label"]),
            True, 
            colorObject[styleObject["shadowColor"]])
        x = labelObject["x"] + colorObject["shadowOffset"]
        y = labelObject["y"] + colorObject["shadowOffset"]
        target.blit(labelSurface, 
            (x, y)) 
        #### #### #### ####
        #### #### #### ####
        #### #### #### ####
        realX = labelSurface.get_width()
        labelSurface = fontObject[styleObject["fontValue"]].render(
            str(labelObject["value"]),
            True, 
            colorObject[styleObject["shadowColor"]])
        x = labelObject["x"] + realX + colorObject["shadowOffset"]
        y = labelObject["y"] + colorObject["shadowOffset"]
        target.blit(labelSurface, 
            (x, y)) 
    labelSurface = fontObject[styleObject["fontLabel"]].render(
        str(labelObject["label"]),
        True, 
        colorObject[styleObject["colorLabel"]])
    x = labelObject["x"]
    y = labelObject["y"]
    target.blit(labelSurface, 
        (x, y)) 
    #### #### #### ####
    #### #### #### ####
    #### #### #### ####
    realX = labelSurface.get_width()
    labelSurface = fontObject[styleObject["fontValue"]].render(
        str(labelObject["value"]),
        True, 
        colorObject[styleObject["colorValue"]])
    x = labelObject["x"] + realX
    y = labelObject["y"]
    target.blit(labelSurface, 
        (x, y)) 
        
def drawLabelCentered(target, labelObject, styleObject, colorObject, fontObject):
    # print()
    labelSurface = fontObject[styleObject["fontLabel"]].render(
        str(labelObject["label"]),
        True, 
        colorObject[styleObject["colorLabel"]])
    #### #### #### ####
    #### #### #### ####
    #### #### #### ####
    valueSurface = fontObject[styleObject["fontValue"]].render(
        str(labelObject["value"]),
        True, 
        colorObject[styleObject["colorValue"]])

    x = labelObject["x"] - ((labelSurface.get_width() + valueSurface.get_width()) / 2)
    y = labelObject["y"] - ((labelSurface.get_height() + valueSurface.get_height()) / 2)
    # print()
    if colorObject["shadowOffset"] > 0:
        if labelSurface.get_width() > 0:
            shadowSource = fontObject[styleObject["fontLabel"]].render(
                str(labelObject["label"]),
                True, 
                colorObject[styleObject["shadowColor"]])
            target.blit(labelSurface, 
                (x + colorObject["shadowOffset"], y + colorObject["shadowOffset"])) 
        if valueSurface.get_width() > 0:
            shadowSource = fontObject[styleObject["fontValue"]].render(
                str(labelObject["value"]),
                True, 
                colorObject[styleObject["shadowColor"]])
            target.blit(shadowSource, 
                (x + colorObject["shadowOffset"], y + colorObject["shadowOffset"])) 

    if labelSurface.get_width() > 0:
        target.blit(labelSurface, 
            (x, y)) 
    if valueSurface.get_width() > 0:
        target.blit(valueSurface, 
            (x + labelSurface.get_width(), y)) 
        
def drawObject(target, drawObject, styleObject, colorObject, fontObject):
    if drawObject["Type"] == "gaugeObject":
        drawGauge(target, drawObject, styleObject, colorObject, fontObject)
    elif drawObject["Type"] == "verticalBarObject":
        drawVerticalBar(target, drawObject, styleObject, colorObject, fontObject)
    elif drawObject["Type"] == "horizontalBarObject":
        drawHorizontalBar(target, drawObject, styleObject, colorObject, fontObject)
    elif drawObject["Type"] == "labelObject":
        drawLabel(target, drawObject, styleObject, colorObject, fontObject)
    elif drawObject["Type"] == "labelCenteredObject":
        drawLabelCentered(target, drawObject, styleObject, colorObject, fontObject)
    # elif drawObject["Type"] == "":
    #     (target, drawObject, styleObject, fontObject)

def displayTheme(target, sourceObject, sourceStyle, sourceColors, sourceFont, hwInfoKey, hwInfo):
    for itemName, dict_ in sourceObject.items():
        isTree = True
        try:
            type = sourceObject[itemName]["Type"]
            isTree = False
        except:
            isTree = True

        if isTree == False:
            if itemName == "Name":
                sourceObject[itemName]["value"] = hwInfo[hwInfoKey][itemName]
                     
            drawObject(target, 
                sourceObject[itemName],
                sourceStyle[itemName], 
                sourceColors,
                sourceFont)
                ## NEED TO ADD SUPPORT FOR THE REMAININF OBJECTS
        else:
            for subItemName, dict_ in sourceObject[itemName].items():
                if keyExist(hwInfo[hwInfoKey], itemName) and keyExist(hwInfo[hwInfoKey][itemName], subItemName):
                    if sourceObject[itemName][subItemName]["FullDataDisplay"] == False:
                        number = hwInfo[hwInfoKey][itemName][subItemName]
                        number = number.split(" ")
                        number = int(float(number[0]))
                        sourceObject[itemName][subItemName]["value"] = \
                            setMappedValue(sourceObject[itemName][subItemName]["value"], number)
                    else:
                        sourceObject[itemName][subItemName]["value"] = \
                            hwInfo[hwInfoKey][itemName][subItemName]
                # else:
                #     if sourceObject[sourceObjectItemName][sourceObjectSubItemName]["FullDataDisplay"] == False:
                #         print("FullDataDisplay = False")
                #         number = random.randrange(100)
                #     else:
                #         print("FullDataDisplay = True")
                #         number = str(random.randrange(100)) + " MHz"
                #     sourceObject[sourceObjectItemName][sourceObjectSubItemName]["value"] = number
                # # print(sourceStyle)
                # print(sourceStyle[itemName])
                # print(sourceStyle[itemName][subItemName])
                # # print()
                drawObject(target, 
                    sourceObject[itemName][subItemName], 
                    sourceStyle[itemName][subItemName], 
                sourceColors,
                    sourceFont)


cpuTheme = {
    "colors": "cpuColors.json",
    "controls": "cpuControls.json",
    "style": "cpuStyle.json"
}
cpuThemeFiles = {
    "colors": {
       "file": "cpuColors.json",
       "stamp": None
    },
    "controls": {
       "file": "cpuControls.json",
       "stamp": None
    },
    "style": {
       "file": "cpuStyle.json",
       "stamp": None
    }
}

gpuTheme = {
    "colors": "gpuColors.json",
    "controls": "gpuControls.json",
    "style": "gpuStyle.json"
}
gpuThemeFiles = {
    "colors": {
       "file": "gpuColors.json",
       "stamp": None
    },
    "controls": {
       "file": "gpuControls.json",
       "stamp": None
    },
    "style": {
       "file": "gpuStyle.json",
       "stamp": None
    }
}

ramTheme = {
    "colors": "ramColors.json",
    "controls": "ramControls.json",
    "style": "ramStyle.json"
}
ramThemeFiles = {
    "colors": {
       "file": "ramColors.json",
       "stamp": None
    },
    "controls": {
       "file": "ramControls.json",
       "stamp": None
    },
    "style": {
       "file": "ramStyle.json",
       "stamp": None
    }
}
    
def main():
    backgrounds = os.listdir(str(Path.cwd()) + "/background/vintage/")
    backgroundIndex = 0
    # print("backgrounds =", backgrounds)

    pygame.init()
    screen = pygame.display.set_mode((800, 480))

    frontSurface = pygame.Surface(
        (400, 240), 
        pygame.SRCALPHA)
    # gpuTheme = {
    #     "colors": "gpuColors.json",
    #     "controls": "gpuControls.json",
    #     "style": "gpuStyle.json"
    # }
    for name, dict_ in gpuThemeFiles.items():
        with open(os.path.abspath(os.getcwd()) + "/" + gpuThemeFiles[name]["file"]) as f:
            gpuTheme[name] = json.load(f)
        gpuThemeFiles[name]["stamp"] = os.path.getmtime(os.path.abspath(os.getcwd()) + "/" + gpuThemeFiles[name]["file"])
    for name, dict_ in cpuThemeFiles.items():
        with open(os.path.abspath(os.getcwd()) + "/" + cpuThemeFiles[name]["file"]) as f:
            cpuTheme[name] = json.load(f)
        cpuThemeFiles[name]["stamp"] = os.path.getmtime(os.path.abspath(os.getcwd()) + "/" + cpuThemeFiles[name]["file"])
    startX = 625
    startY = 250
    x = startX
    y = startY
    width = 12
    index = 0
    for i in range(1, 13):
        cpuTheme["controls"]["Loads"]["CPU Core #" + str(i)]["x"] = x + ((width - 1) * (index - 1))
        cpuTheme["controls"]["Loads"]["CPU Core #" + str(i)]["width"] = width
        index = index + 1

    for name, dict_ in ramThemeFiles.items():
        with open(os.path.abspath(os.getcwd()) + "/" + ramThemeFiles[name]["file"]) as f:
            ramTheme[name] = json.load(f)
        ramThemeFiles[name]["stamp"] = os.path.getmtime(os.path.abspath(os.getcwd()) + "/" + ramThemeFiles[name]["file"])

    # themeData = []  
    # with open(os.path.abspath(os.getcwd()) + "/" + themeFolder + "/theme.json") as f:
    #     themeData = json.load(f)
    
    for name, dict_ in themeFonts.items():
        themeFonts[name] = createLocalFont(themeFonts[name], os.path.abspath(os.getcwd()) + "/fonts/")

    backgroundIndex = 0
    animatedBackIndex = 0
    animatedBack = loadGIF(str(Path.cwd()) + "/background/vintage/" + backgrounds[backgroundIndex])

    while 1:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                return
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    backgroundIndex = backgroundIndex - 1
                    animatedBackIndex = 0
                    if backgroundIndex < 0:
                        backgroundIndex = len(backgrounds) - 1
                    animatedBack = loadGIF(str(Path.cwd()) + "/background/vintage/" + backgrounds[backgroundIndex])
                elif event.key == pygame.K_RIGHT:
                    backgroundIndex = backgroundIndex + 1
                    animatedBackIndex = 0
                    if backgroundIndex >= len(backgrounds):
                        backgroundIndex = 0
                    animatedBack = loadGIF(str(Path.cwd()) + "/background/vintage/" + backgrounds[backgroundIndex])
                    
                elif event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    return
                
        for name, dict_ in gpuThemeFiles.items():
            newStamp = os.path.getmtime(os.path.abspath(os.getcwd()) + "/" + gpuThemeFiles[name]["file"])
            if gpuThemeFiles[name]["stamp"] != \
                newStamp:
                with open(os.path.abspath(os.getcwd()) + "/" + gpuThemeFiles[name]["file"]) as f:
                    gpuTheme[name] = json.load(f)
                gpuThemeFiles[name]["stamp"] = os.path.getmtime(os.path.abspath(os.getcwd()) + "/" + gpuThemeFiles[name]["file"])
        for name, dict_ in cpuThemeFiles.items():
            newStamp = os.path.getmtime(os.path.abspath(os.getcwd()) + "/" + cpuThemeFiles[name]["file"])
            if cpuThemeFiles[name]["stamp"] != \
                newStamp:
                with open(os.path.abspath(os.getcwd()) + "/" + cpuThemeFiles[name]["file"]) as f:
                    cpuTheme[name] = json.load(f)
                cpuThemeFiles[name]["stamp"] = os.path.getmtime(os.path.abspath(os.getcwd()) + "/" + cpuThemeFiles[name]["file"])
        for name, dict_ in ramThemeFiles.items():
            newStamp = os.path.getmtime(os.path.abspath(os.getcwd()) + "/" + ramThemeFiles[name]["file"])
            if ramThemeFiles[name]["stamp"] != \
                newStamp:
                with open(os.path.abspath(os.getcwd()) + "/" + ramThemeFiles[name]["file"]) as f:
                    ramTheme[name] = json.load(f)
                ramThemeFiles[name]["stamp"] = os.path.getmtime(os.path.abspath(os.getcwd()) + "/" + ramThemeFiles[name]["file"])
        # hwInfo = getHwInfo()
        # # jsonObj = json.dumps(hwInfo["GPU"]["LOADS"], indent=2)
        # # print(hwInfo["GPU"]["LOADS"])

        renderImage(0, 0, screen.get_width(), screen.get_height(), screen, animatedBack[animatedBackIndex])
        # renderImage(0, 0, screen.get_width(), screen.get_height(), screen, background)

        frontSurface.fill(Color(0x0, 0x0, 0x0, 0x0))

        animatedBackIndex = animatedBackIndex + 1;
        if(animatedBackIndex >= len(animatedBack)):
            animatedBackIndex = 0;
        
        hwInfo = gethardwareInfo()
        # jsonObj = json.dumps(hwInfo, indent=2)
        # print(jsonObj)

        fpss = 0
        try:
            fpss = hwInfo["GPU"]["Factor"]["Fullscreen FPS"]
            fpss = fpss.split(" ")
            fpss = int(float(fpss[0]))
            if fpss > 0:
                gpuTheme["controls"]["Loads"]["D3D 3D"]["enableValue"] = False
            else:
                gpuTheme["controls"]["Loads"]["D3D 3D"]["enableValue"] = True
        except:
            pass
        
        
        # jsonObj = json.dumps(hwInfo["CPU"], indent=2)
        # print(jsonObj)
        displayTheme(frontSurface, 
            cpuTheme["controls"], 
            cpuTheme["style"], 
            cpuTheme["colors"], 
            themeFonts, 
            "CPU", hwInfo)
        displayTheme(frontSurface, 
            ramTheme["controls"], 
            ramTheme["style"], 
            ramTheme["colors"], 
            themeFonts, 
            "MEMORY", hwInfo)

        for gpuItemName, dict_ in gpuTheme["controls"].items():
            isTree = True
            try:
                type = gpuTheme["controls"][gpuItemName]["Type"]
                isTree = False
            except:
                isTree = True

            if isTree == False:
                if gpuItemName == "Name":
                    gpuTheme["controls"][gpuItemName]["value"] = hwInfo["GPU"]["Name"]
                     
                drawObject(frontSurface, 
                    gpuTheme["controls"][gpuItemName], 
                    gpuTheme["style"][gpuItemName], 
                    gpuTheme["colors"],
                    themeFonts)
            else:
                for gpuSubItemName, dict_ in gpuTheme["controls"][gpuItemName].items():
                    if gpuItemName == "Factor" and gpuSubItemName == "Fullscreen FPS" and fpss <= 0:
                        pass
                    # if 0 == 1:
                    #     pass
                    else:
                        if keyExist(hwInfo["GPU"], gpuItemName) and keyExist(hwInfo["GPU"][gpuItemName], gpuSubItemName):
                            if gpuTheme["controls"][gpuItemName][gpuSubItemName]["FullDataDisplay"] == False:
                                number = hwInfo["GPU"][gpuItemName][gpuSubItemName]
                                number = number.split(" ")
                                number = int(float(number[0]))
                                gpuTheme["controls"][gpuItemName][gpuSubItemName]["value"] = \
                                    setMappedValue(gpuTheme["controls"][gpuItemName][gpuSubItemName]["value"], number)
                            else:
                                gpuTheme["controls"][gpuItemName][gpuSubItemName]["value"] = hwInfo["GPU"][gpuItemName][gpuSubItemName]
                        else:
                            number = random.randrange(100)
                            if gpuTheme["controls"][gpuItemName][gpuSubItemName]["FullDataDisplay"] == False:
                                gpuTheme["controls"][gpuItemName][gpuSubItemName]["value"] = \
                                    setMappedValue(gpuTheme["controls"][gpuItemName][gpuSubItemName]["value"], number)
                            else:
                                gpuTheme["controls"][gpuItemName][gpuSubItemName]["value"] = \
                                    str(number) + "." + str(random.randrange(99)) + " MHz"

                        drawObject(frontSurface, 
                            gpuTheme["controls"][gpuItemName][gpuSubItemName], 
                            gpuTheme["style"][gpuItemName][gpuSubItemName], 
                            gpuTheme["colors"],
                            themeFonts)

        time.sleep(0.050)
        
        scaledOutput = pygame.transform.scale(frontSurface, (800, 480))
        screen.blit(scaledOutput, 
            (0, 0))
        
        pygame.display.flip()

        # time.sleep(0.10)

if __name__ == "__main__":
    main()
    
