import clr #package pythonnet, not clr
import os
from pathlib import Path

openhardwaremonitor_hwtypes = ['Mainboard','SuperIO','CPU','RAM','GpuNvidia','GpuAti','TBalancer','Heatmaster','HDD']
cputhermometer_hwtypes = ['Mainboard','SuperIO','CPU','GpuNvidia','GpuAti','TBalancer','Heatmaster','HDD']
openhardwaremonitor_sensortypes = ['Voltage','Clock','Temperature','Load','Fan','Flow','Control','Level','Factor','Power','Data','SmallData']
cputhermometer_sensortypes = ['Voltage','Clock','Temperature','Load','Fan','Flow','Control','Level']


def initialize_openhardwaremonitor():
    file = str(Path.cwd()) + r"\OpenHardwareMonitorLib.dll"
    clr.AddReference(file)

    from OpenHardwareMonitor import Hardware

    handle = Hardware.Computer()
    handle.MainboardEnabled = True
    handle.CPUEnabled = True
    handle.GPUEnabled = True
    handle.RAMEnabled = True
    handle.HDDEnabled = True
    handle.Open()
    return handle

def initialize_cputhermometer():
    file = str(Path.cwd()) + r"\CPUThermometerLib.dll"
    clr.AddReference(file)

    from CPUThermometer import Hardware
    handle = Hardware.Computer()
    handle.CPUEnabled = True
    handle.Open()
    return handle

def fetch_stats(handle):
    for i in handle.Hardware:
        i.Update()
        for sensor in i.Sensors:
            parse_sensor(sensor)
        for j in i.SubHardware:
            j.Update()
            for subsensor in j.Sensors:
                parse_sensor(subsensor)


def parse_sensor(sensor):
        if sensor.Value is not None:
            if type(sensor).__module__ == 'CPUThermometer.Hardware':
                sensortypes = cputhermometer_sensortypes
                hardwaretypes = cputhermometer_hwtypes
            elif type(sensor).__module__ == 'OpenHardwareMonitor.Hardware':
                sensortypes = openhardwaremonitor_sensortypes
                hardwaretypes = openhardwaremonitor_hwtypes
            else:
                return

            if sensor.SensorType == sensortypes.index('Temperature'):
                print(u"%s %s Temperature Sensor #%i %s - %s\u00B0C" % (hardwaretypes[sensor.Hardware.HardwareType], sensor.Hardware.Name, sensor.Index, sensor.Name, sensor.Value))

if __name__ == "__main__":
    import clr # the pythonnet module.
    import json

    file = str(Path.cwd()) + r"\OpenHardwareMonitorLib"
    clr.AddReference(file) 
    # e.g. clr.AddReference(r'OpenHardwareMonitor/OpenHardwareMonitorLib'), without .dll

    from OpenHardwareMonitor.Hardware import Computer

    c = Computer()
    c.MainboardEnabled = True
    c.CPUEnabled = True # get the Info about CPU
    c.GPUEnabled = True # get the Info about GPU
    c.RAMEnabled = True
    c.HDDEnabled = True
    c.Open()
    # while True:
    #     for a in range(0, len(c.Hardware[0].Sensors)):
    #         # print(c.Hardware[0].Sensors[a].Identifier)

    #         # if "/temperature" in str(c.Hardware[0].Sensors[a].Identifier):
    #         #     print(c.Hardware[0].Sensors[a].get_Value())
    #         #     c.Hardware[0].Update()

    print("MAINBOARD ---- ---- ---- ---- ---- ----")
    print(c.Hardware[0].Name)
    print(c.Hardware[0].Identifier)
    print("Sensors: ", len(c.Hardware[0].Sensors))
    # print(json.dumps(c.Hardware[0]))
    c.Hardware[0].Update()
    # for a in range(0, len(c.Hardware[0].FanControllersSensors)):
    #     print(str(c.Hardware[0].Sensors[a].Identifier))

    for a in range(0, len(c.Hardware[0].Sensors)):
        print(str(c.Hardware[0].Sensors[a].Identifier))
        print(c.Hardware[0].Sensors[a].get_Value())

    print("CPU ---- ---- ---- ---- ---- ----")
    c.Hardware[1].Update()
    for a in range(0, len(c.Hardware[1].Sensors)):
        print(str(c.Hardware[1].Sensors[a].Identifier))
        print(c.Hardware[1].Sensors[a].get_Value())

    print("RAM ---- ---- ---- ---- ---- ----")
    c.Hardware[2].Update()
    for a in range(0, len(c.Hardware[2].Sensors)):
        print(str(c.Hardware[2].Sensors[a].Identifier))
        print(c.Hardware[2].Sensors[a].get_Value())

    print("GPU ---- ---- ---- ---- ---- ----")
    c.Hardware[3].Update()
    for a in range(0, len(c.Hardware[3].Sensors)):
        print(str(c.Hardware[3].Sensors[a].Identifier))
        print(c.Hardware[3].Sensors[a].get_Value())

    print("HDD ---- ---- ---- ---- ---- ----")
    c.Hardware[4].Update()
    for a in range(0, len(c.Hardware[4].Sensors)):
        print(str(c.Hardware[4].Sensors[a].Identifier))
        print(c.Hardware[4].Sensors[a].get_Value())
