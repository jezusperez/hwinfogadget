import io
import os
import signal
import subprocess
import uuid
import time
import re
import platform
import json
import sys
import threading 

from uartDevice import uartDevice
from infi.systray import SysTrayIcon
from systeminfo import getHwInfo

ENABLE_LOG = True
def logMessage(*args):
    if ENABLE_LOG is True:
        for arg in args:
            print("hwInfoGadgetService Module:", arg)

class hwMonitor:
    m_SerialEnabled = False;
    m_SerialPortName = "";
    m_SerialPortSpeed = "";
    m_SerialPort = None;
    m_BufferSize = 1024;
    m_Buffer = bytearray(m_BufferSize);

    m_TerminateFlag = False
    m_UartOutput = uartDevice(None, 115200);

    m_UartThread = None

    def __init__(self):
        with open(os.path.abspath(os.getcwd()) + "/" + "/uartConfig.json") as f:
            uartData = json.load(f)
            self.m_SerialPortName = uartData["port"];
            self.m_SerialPortSpeed = uartData["baud"];
        m_TerminateFlag = False

    def start(self):
        logMessage("start()")
        self.m_SerialEnabled = False;
        self.m_UartOutput = uartDevice(self.m_SerialPortName, self.m_SerialPortSpeed);
        logMessage("Open UART")
        self.m_UartOutput.open()
        self.m_SerialEnabled = self.m_UartOutput.isEnabled()

    def runThread(self):
        logMessage("runThread")
        self.m_TerminateFlag = False
        while(not self.m_TerminateFlag):
            logMessage("runThread loop")
            logMessage("self.m_SerialEnabled =", self.m_SerialEnabled )
            hwInfo = getHwInfo()
            # jsonObj = json.dumps(hwInfo, indent=2)
            # print(jsonObj)
            jsonHwInfo = json.dumps(hwInfo)
            logMessage("jsonHwInfo", jsonHwInfo)
            jsonHwInfo = jsonHwInfo.replace(' %', '')
            jsonHwInfo = jsonHwInfo.replace(' \\u00b0C', '')
            jsonHwInfo = jsonHwInfo.replace(' MHz', '')

#             jsonHwInfo = bytes(jsonHwInfo, 'utf-8')
# #             # jsonHwInfo = jsonHwInfo.replace('CPU Core ', 'CPU')
# #             # jsonHwInfo = jsonHwInfo.replace('CPU Total', 'CPU')
# #             # jsonHwInfo = jsonHwInfo.replace('GPU ', '')
# #             # jsonHwInfo = jsonHwInfo.replace(' Memory', '')
# #             # jsonHwInfo = jsonHwInfo.replace('Fullscreen ', '')
# #             # jsonHwInfo = jsonHwInfo.replace('Memory', 'Mem')
# #             # # jsonHwInfo = jsonHwInfo.replace('TEMPERATURES', 'TEMP')
# #             # jsonHwInfo = jsonHwInfo.replace('D3D 3D', 'D3D')
# #             # jsonHwInfo = jsonHwInfo.replace('Name', '')
# #             logMessage("jsonHwInfo", jsonHwInfo)
# #             # logMessage("runThread loop")
# #             jsonData = json.loads(jsonHwInfo)
# #             print(len(json.dumps(jsonData["HW"])))
# #             print(json.dumps(jsonData["LOADS"]))
# #             print(len(json.dumps(jsonData["TEMPERATURES"])))
# #             print(len(json.dumps(jsonData["CLOCKS"])))
# #             outputData = []
# #             outputData.append(jsonData["HW"]["CPUName"])
# #             outputData.append(jsonData["HW"]["GPUName"])
# #             outputData.append(jsonData["LOADS"]["CPU Core #1"])
# #             outputData.append(jsonData["LOADS"]["CPU Core #2"])
# #             outputData.append(jsonData["LOADS"]["CPU Core #3"])
# #             outputData.append(jsonData["LOADS"]["CPU Core #4"])
# #             outputData.append(jsonData["LOADS"]["CPU Core #5"])
# #             outputData.append(jsonData["LOADS"]["CPU Core #6"])
# # "CPU Core #7"])
# # "CPU Core #8"])
# # "CPU Core #9"])
# # "CPU Core #10"])
# # "CPU Core #11"])
# # "CPU Core #12"])
# # "CPU Total"])
# # "Memory"])
# # "Virtual Memory"])
# # "Fullscreen FPS"])
# # "D3D 3D"])
# # "GPU Core"])
# # "GPU Memory"])
#             # logMessage(json.dumps({"PAYLOAD":outputData}))
#             # logMessage(len(json.dumps(jsonData["HW"])))
            if self.m_SerialEnabled == True:
                logMessage("Send message")
                self.m_UartOutput.sendMessage(jsonHwInfo)
                # self.m_UartOutput.sendMessage(json.dumps(jsonData["HW"]))
                # self.m_UartOutput.sendMessage(json.dumps(jsonData["LOADS"]))
                # self.m_UartOutput.sendMessage(json.dumps(jsonData["TEMPERATURES"]))
                # self.m_UartOutput.sendMessage(json.dumps(jsonData["CLOCKS"]))
            time.sleep(0.5)
        
    def run(self):
        self.m_UartThread = threading.Thread (target = self.runThread, args = ())  
        self.m_UartThread.start ()

    def terminate(self):
        self.m_TerminateFlag = True
        self.m_UartThread.join ()
        self.m_UartOutput.close()

app = hwMonitor()

def reloadApp(systray):
    app = hwMonitor()
    
def on_quit_callback(systray):
    app.terminate()

if __name__ == '__main__':

    app.start()

    menu_options = (("hwInfo Gadget Service", None, reloadApp),)
    systray = SysTrayIcon("logo.ico", "hwInfo Gadget Service", menu_options, on_quit=on_quit_callback)
    systray.start()

    app.run()

    systray.shutdown()
   

    # epiz_27668145
    # YB%lTmz8bsJ5w2f&CQ8*