import psutil
import cpuinfo
import platform

cpuInfo = {}
processingInfo = {}

def convertBytes(bytes, suffix="B"):
    """
    Scale bytes to its proper format
    e.g:
        1253656 => '1.20MB'
        1253656678 => '1.17GB'
    """
    factor = 1024
    for unit in ["", "K", "M", "G", "T", "P"]:
        if bytes < factor:
            return f"{bytes:.2f}{unit}{suffix}"
        bytes /= factor

def getInfo():
    cpuInfo = {}
    uname = platform.uname()
    cpuInfo["SYSTEM"] = uname.system
    cpuInfo["NODENAME"] = uname.node
    cpuInfo["RELEASE"] = uname.release
    cpuInfo["VERSION"] = uname.version
    cpuInfo["MACHINE"] = uname.machine
    cpuInfo["PROCESSOR"] = uname.processor
    cpuInfo["PROCESSOR_BRAND"] = cpuinfo.get_cpu_info()['brand_raw']

    cpuInfo["PHYSICAL_CORES"] = psutil.cpu_count(logical=False)
    cpuInfo["TOTAL_CORES"] = psutil.cpu_count(logical=True)
    
    cpufreq = psutil.cpu_freq()
    cpuInfo["MAX_FREQUENCY_MHZ"] = f"{cpufreq.max:.2f}"
    cpuInfo["MIN_FREQUENCY_MHZ"] = f"{cpufreq.min:.2f}"

    svmem = psutil.virtual_memory()
    cpuInfo["MEMORY"] = f"{svmem.total}"

    cpuInfo["DRIVES"] = []
    partitions = psutil.disk_partitions()
    for partition in partitions:
        try:
            partition_usage = psutil.disk_usage(partition.mountpoint)
        except PermissionError:
            # this can be catched due to the disk that
            # isn't ready
            continue
        cpuInfo["DRIVES"].append({"NAME": f"{partition.device}", "MOUNT": f"{partition.mountpoint}", "SIZE": f"{partition_usage.total}"})

    print("---- ---- ---- ---- ---- ")
    print(uname)
    print("---- ---- ---- ---- ---- ")
    print(psutil)
    print("---- ---- ---- ---- ---- ")

    return cpuInfo

def getProcessing():
    processingInfo = {}

    cpufreq = psutil.cpu_freq()
    svmem = psutil.virtual_memory()

    processingInfo["FREQUENCY_MHZ"] = f"{cpufreq.current:.2f}"
    # CPU usage
    for i, percentage in enumerate(psutil.cpu_percent(percpu=True)):
        processingInfo[f"CORE_{i}_LOAD_PERCENT"] = f"{percentage}"
        
    processingInfo["LOAD_PERCENT"] = f"{psutil.cpu_percent()}"
    processingInfo["RAM_AVAILABLE"] = f"{svmem.available}"
    processingInfo["RAM_USED"] = f"{svmem.used}"
    processingInfo["RAM_PERCENTAGE"] = f"{svmem.percent}"

    processingInfo["DRIVES"] = []
    partitions = psutil.disk_partitions()
    for partition in partitions:
        try:
            partition_usage = psutil.disk_usage(partition.mountpoint)
        except PermissionError:
            continue
        processingInfo["DRIVES"].append({"NAME": f"{partition.device}", "MOUNT": f"{partition.mountpoint}", "SIZE": f"{partition_usage.total}", "USED": f"{partition_usage.used}", "FREE": f"{partition_usage.free}", "PERCENTAGE": f"{partition_usage.percent}"})

    net_io = psutil.net_io_counters()
    processingInfo["NETWORK"] = {"SENT": f"{net_io.bytes_sent}", "RECEIVED": f"{net_io.bytes_recv}"}

    return processingInfo

if __name__ == "__main__":
    print(getInfo())
    print(getProcessing())
