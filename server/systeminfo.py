# coding: utf-8

import json
import time
import socket
from threading import Timer, Lock
from typing import List
from wsgiref import simple_server
from HardwareMonitor.Util import OpenComputer, HardwareTypeString, SensorTypeString, SensorValueToString, GroupSensorsByType
from HardwareMonitor.Hardware import SensorType, IComputer, IHardware, ISensor


# ------------------------------------------------------------------------------
SensorTypeStringPlurals = {
    SensorType.Voltage: "Voltages",
    SensorType.Current: "Currents",
    SensorType.Clock: "Clocks",
    SensorType.Load: "Loads",
    SensorType.Temperature: "Temperatures",
    SensorType.Fan: "Fans",
    SensorType.Level: "Levels",
    SensorType.Power: "Powers",
    SensorType.Frequency: "Frequencies",
}

# ------------------------------------------------------------------------------
class NodeFormatter():
    _counter = 0
    def getId(self):
        node_id = self._counter
        self._counter += 1
        return node_id

    def _makeNode(self, Text, Min="", Value="", Max="", ImageURL="", **kwargs):
        return dict(id=self.getId(), Text=Text, Min=Min, Value=Value, Max=Max,
                    ImageURL=ImageURL, Children=[], **kwargs)

    def makeSensorGroupNode(self, sensors: List[ISensor]):
        sensor_type = sensors[0].SensorType
        type_str    = SensorTypeString[sensor_type]
        def makeSensorNode(sensor: ISensor):
            min_str = SensorValueToString(sensor.Min,    sensor_type)
            val_str = SensorValueToString(sensor.Value,  sensor_type)
            max_str = SensorValueToString(sensor.Max,    sensor_type)
            return self._makeNode(sensor.Name, Min=min_str, Value=val_str, Max=max_str, Type=type_str,
                                  SensorId=sensor.Identifier.ToString())

        group_node = self._makeNode(SensorTypeStringPlurals.get(sensor_type, type_str))
        group_node["Children"].extend(map(makeSensorNode, sensors))
        return group_node

    def makeHardwareNode(self, hardware: IHardware):
        sensors_grouped = GroupSensorsByType(hardware.Sensors)
        hardware_type   = HardwareTypeString[hardware.HardwareType]
        hardware_node   = self._makeNode(hardware.Name, Type=hardware_type)
        hardware_node["Children"].extend(map(self.makeSensorGroupNode, sensors_grouped))
        hardware_node["Children"].extend(map(self.makeHardwareNode, hardware.SubHardware))
        return hardware_node

    def buildNodeTree(self, computer: IComputer):
        self._counter = 0
        root_node = self._makeNode("Sensor")
        computer_node = self._makeNode(socket.gethostname())

        root_node["Children"].append(computer_node)
        computer_node["Children"].extend(map(self.makeHardwareNode, computer.Hardware))
        return root_node



# ------------------------------------------------------------------------------
class IndefiniteTimer(Timer):
    def start(self):
        self.daemon = True
        super().start()
        return self

    def run(self):
        delay = self.interval
        while not self.finished.wait(delay):
            start_time = time.perf_counter()
            self.function(*self.args, **self.kwargs)
            delay = max(0, self.interval - (time.perf_counter() - start_time))


# ------------------------------------------------------------------------------
class SensorApp():
    def __init__(self, port=8085, interval=1.0):
        self.interval = interval
        self.mutex    = Lock()
        self.computer = OpenComputer(all=True, time_window=interval)
        self.timer    = IndefiniteTimer(interval, self.update).start()
        self.http     = simple_server.make_server('', port, self.handler)

    @property
    def port(self):
        return self.http.server_port

    def update(self):
        with self.mutex:
            self.computer.Update()

    def getSensors(self):
        with self.mutex:
            return NodeFormatter().buildNodeTree(self.computer)

    def serve(self):
        self.http.serve_forever()

    def close(self):
        self.timer.cancel()
        self.http.shutdown()

    def handler(self, environ, respond):
        if environ['PATH_INFO'].lower() == "/data.json":
            json_str = json.dumps(self.getSensors(), ensure_ascii=False)
            respond('200 OK', [('Content-Type', 'application/json')])
            return [json_str.encode("utf-8")]
        else:
            respond('404 Not Found', [('Content-Type', 'application/json')])
            return [b'not found']
        
computer = None
computer = OpenComputer(all=True)
def getHwInfo():
    computer.Update()
    computerTree = NodeFormatter().buildNodeTree(computer)

    # json_formatted_str = json.dumps(computerTree, indent=2)
    # print(computerTree["Text"])
    # print(len(computerTree["Children"]))
    # print(len(computerTree["Children"][0]))
    # print("---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")
    # print(json_formatted_str);
    # print("---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")
    # quit()

    # GPUName = ""
    # GPUTemperatures = []
    # GPULoads = {}
    # GPUFps = []
    # GPUClocks = []

    # CPUName = ""
    # CPULoads = []
    # MemoryLoads = []

    hwNames = {}
    hwLoads = {}
    hwTemperatures = {}
    hwFactor = {}
    hwClocks = {}

    hwInfo = {}

    for child in computerTree["Children"][0]["Children"]:
        # print("child[Type] =", child["Type"])
        if child["Type"] == "Memory":
            # print("---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")
            # CPUName = child["Text"]
            # print(CPUName)
            
            # jsonObj = json.dumps(child, indent=2)
            # print(jsonObj)

            for grandChild in child["Children"]:
                # print("grandChild[Text] =", grandChild["Text"])
                if(grandChild["Text"] == "Loads"):
                    for grandGrandChild in grandChild["Children"]:
                        # # print(grandGrandChild)
                        # # MemoryLoads.append({
                        # #     "ID": grandGrandChild["Text"],
                        # #     "VALUE": grandGrandChild["Value"]
                        # #     })
                        # hwLoads[grandGrandChild["Text"]] = grandGrandChild["Value"]
                        hwLoads[grandGrandChild["Text"]] = int(float(''.join(c for c in grandGrandChild["Value"] if c.isdigit())))
                # if(grandChild["Text"] == "Loads"):
                #     CPULoads.append({
                #         "ID": grandChild["Text"],
                #         "VALUE": grandChild["Value"]
                #     })
                    # jsonObj = json.dumps(Memoryloads, indent=2)
                    # print(jsonObj)
            
        if child["Type"] == "Cpu":
            # print("---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")
            # CPUName = child["Text"]
            hwNames["CPUName"] = child["Text"]
            # print(CPUName)
            
            # jsonObj = json.dumps(child, indent=2)
            # print(jsonObj)

            for grandChild in child["Children"]:
                if(grandChild["Text"] == "Loads"):
                    for grandGrandChild in grandChild["Children"]:
                        # # print(grandGrandChild["Text"])
                        # # print(grandGrandChild["Text"].find("CPU Core #"))
                        # if grandGrandChild["Text"].find("CPU Core #") < 0:
                        #     hwLoads[grandGrandChild["Text"]] = int(float(''.join(c for c in grandGrandChild["Value"] if c.isdigit())))
                        hwLoads[grandGrandChild["Text"]] = int(float(''.join(c for c in grandGrandChild["Value"] if c.isdigit())))
                        # CPULoads.append({
                        #     "ID": grandGrandChild["Text"],
                        #     "VALUE": grandGrandChild["Value"]
                        # })
            # jsonObj = json.dumps(CPULoads, indent=2)
            # print(jsonObj)

        if child["Type"] == "GpuAmd" \
        or child["Type"] == "GpuNvidia" \
        or child["Type"] == "GpuIntel":
            # print("---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")
            # jsonObj = json.dumps(child, indent=2)
            # print(jsonObj)
            # print("---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")
            # return
            # GPUName = child["Text"]
            hwNames["GPUName"] = child["Text"]
            
            for grandChild in child["Children"]:
                if(grandChild["Text"] == "Temperatures"):
                    # jsonObj = json.dumps(grandChild, indent=2)
                    # print(jsonObj)
                    for grandGrandChild in grandChild["Children"]:
                        # # GPUTemperatures.append({
                        # #     "ID": grandGrandChild["Text"],
                        # #     "VALUE": grandGrandChild["Value"]
                        # # })
                        # hwTemperatures[grandGrandChild["Text"]] = grandGrandChild["Value"];
                        if grandGrandChild["Text"].find("GPU VR VDDC") < 0:
                            hwTemperatures[grandGrandChild["Text"]] = int(float(''.join(c for c in grandGrandChild["Value"] if c.isdigit())))
                    # print("Temperatures ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")
                    # print(GPUTemperatures)
                elif(grandChild["Text"] == "Factor"):
                    for grandGrandChild in grandChild["Children"]:
                        if grandGrandChild["Text"] == "Fullscreen FPS":
                            # # GPULoads.append({
                            # #     "ID": grandGrandChild["Text"],
                            # #     "VALUE": grandGrandChild["Value"]
                            # # })
                            # # GPULoads[grandGrandChild["Text"]] = grandGrandChild["Value"];
                            # hwLoads[grandGrandChild["Text"]] = grandGrandChild["Value"]
                            hwLoads[grandGrandChild["Text"]] = str(int(float(''.join(c for c in grandGrandChild["Value"] if c.isdigit()))))
                elif(grandChild["Text"] == "Loads"):
                    for grandGrandChild in grandChild["Children"]:
                        if grandGrandChild["Text"] == "GPU Core" \
                            or grandGrandChild["Text"] == "D3D 3D" \
                            or grandGrandChild["Text"] == "GPU Memory":
                            # # GPULoads.append({
                            # #     "ID": grandGrandChild["Text"],
                            # #     "VALUE": grandGrandChild["Value"]
                            # # })
                            # # GPULoads[grandGrandChild["Text"]] = grandGrandChild["Value"];
                            # hwLoads[grandGrandChild["Text"]] = grandGrandChild["Value"]
                            hwLoads[grandGrandChild["Text"]] = int(float(''.join(c for c in grandGrandChild["Value"] if c.isdigit())))
                    # print("Loads ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")
                    # print(GPULoads)
                    # print(child["Text"])
                elif(grandChild["Text"] == "Factor"):
                    # jsonObj = json.dumps(grandChild, indent=2)
                    # print(jsonObj)
                    for grandGrandChild in grandChild["Children"]:
                        if grandGrandChild["Text"] == "Fullscreen FPS":
                            GPUFps.append({
                                "ID": grandGrandChild["Text"],
                                # "VALUE": grandGrandChild["Value"]
                                "VALUE": str(int(float(''.join(c for c in grandGrandChild["Value"] if c.isdigit()))))
                            })
                    # print("Fps ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")
                    # print(GPUFps)

                elif(grandChild["Text"] == "Clocks"):
                    # jsonObj = json.dumps(grandChild, indent=2)
                    # print(jsonObj)
                    for grandGrandChild in grandChild["Children"]:
                        if grandGrandChild["Text"] == "GPU Core" \
                           or grandGrandChild["Text"] == "GPU Memory" \
                           or grandGrandChild["Text"] == "GPU Memory Controller":
                            # GPUClocks.append({
                            #     "ID": grandGrandChild["Text"],
                            #     "VALUE": grandGrandChild["Value"]
                            # })
                            
                            # hwClocks[grandGrandChild["Text"]] = grandGrandChild["Value"]
                            hwClocks[grandGrandChild["Text"]] = int(float(''.join(c for c in grandGrandChild["Value"] if c.isdigit())))
            #         print("Clocks ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")
            #         print(GPUClocks)
            # print("---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")
            # jsonObj = json.dumps(child, indent=2)
            # print(jsonObj)
            # print("---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")

    #         jsonObj = json.dumps(child, indent=2)
    #         print(jsonObj)
    # # print(json_formatted_str)
    hwInfo["HW"] = hwNames
    hwInfo["LOADS"] = hwLoads
    hwInfo["TEMPERATURES"] = hwTemperatures
    hwInfo["CLOCKS"] = hwClocks
        # "GPU": {
        #     "NAME": GPUName,
        #     "TEMPERATURES": GPUTemperatures,
        #     "CLOCKS": GPUClocks,
        #     "LOADS": GPULoads
        # },
        # "CPU": {
        #     "NAME": CPUName,
        #     "LOADS": CPULoads,
        #     "MEMORY": MemoryLoads
        # }
    
    # jsonObj = json.dumps(hwInfo, indent=2)
    # print(jsonObj)
    return hwInfo
    # print("GPUName =", GPUName);
    # print("---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")
    # print("GPUTemperatures =", GPUTemperatures);
    # print("---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")
    # print("GPULoads =", GPULoads);
    # print("---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")
    # print("GPUFps =", GPUFps);
    # print("---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")
    # print("GPUClocks =", GPUClocks);
    # print("---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")
    # print("CPUName =", CPUName);
    # print("---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")
    # print("CPULoads =", CPULoads);
    # print("---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ")
    # print("MemoryLoads =", MemoryLoads);

if __name__ == "__main__":
    # main()
    hwInfo = getHwInfo()
    jsonObj = json.dumps(hwInfo, indent=2)
    print(jsonObj)
    