
from os import listdir;
from os.path import isfile, join;
import random;

class p9scrambler:
    bufferSize = 512;

    # def __init__(self):
    #     pass;

    def __init__(self, bufferSize):
        self.bufferSize = bufferSize;
        pass;

    def scramble(self, message):
        # buffer = str.encode(message);
        buffer_len = len(message);

        # print(message);
        # print(buffer);

        output = bytearray();
        for index in range(0, self.bufferSize, 1):
            output.append(random.randrange(0, 255, 3));

        # output.append(random.randrange(1, 255, 3));
        # output.append(buffer_len ^ output[0]);
        # for index in range(0, buffer_len, 1):
        #     # print(buffer[index], buffer[index] ^ output[0]);
        #     output.append(message[index] ^ output[0]);
        ##output.append(random.randrange(1, 255, 3));
        output[1] = (buffer_len ^ output[0]);
        for index in range(0, buffer_len, 1):
            output[2 + index] = (message[index] ^ output[0]);
    
        return output;

    def descramble(self, message):
        outputMessage = bytearray();
        for index in range(0, message[1] ^ message[0], 1):
            # print(output[index + 2] ^ output[0]);
            outputMessage.append(message[index + 2] ^ message[0]);
        return outputMessage;

